FROM nginx:1.14.2-alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY build/ .
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
