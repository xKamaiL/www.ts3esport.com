import { requestWithToken } from '../config/request'

const ServerService = {
  getMyServers: () => requestWithToken.get('server'),
  findById: id => requestWithToken.get('server/' + id),
  manage: requestWithToken
}

export default ServerService
