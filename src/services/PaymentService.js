import { requestWithToken } from '../config/request'

const PaymentService = {
  history: params => requestWithToken.get('/topup', { params }),
  truemoney: truemoney =>
    requestWithToken.post('/topup/truemoney', {
      truemoney: truemoney
    })
}

export default PaymentService
