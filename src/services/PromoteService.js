import { requestWithToken } from '../config/request'

const PromoteService = {
  create: body => requestWithToken.post('/promote', body),
  me: () => requestWithToken.get('/promote/me'),
  update: (id, body) =>
    requestWithToken.post('/promote/' + id + '/update', body),
  delete: id => requestWithToken.delete('/promote/' + id),
  findPromoteByGameId: (game_id, params) =>
    requestWithToken.get('/promote/' + game_id, { params }),
  fetchDetails: id => requestWithToken.get('/promote/details/' + id),
  voteServer: (server_id, body) =>
    requestWithToken.post('/promote/vote/' + server_id, body),
  voteScore: () => requestWithToken.get('/promote'),
  topTensRanking: () => requestWithToken.get('/promote/top_ranking'),
  latest: () => requestWithToken.get('/promote/latest')
}

export default PromoteService
