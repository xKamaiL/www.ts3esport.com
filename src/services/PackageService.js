import { API_URL_NEW, requestWithToken } from '../config/request'
import { getFormData } from '../utils'

const PackageService = {
    findAllPackage: () => requestWithToken.get('plan', {
        baseURL: API_URL_NEW
    }),
    findAllServers: () => requestWithToken.get('plan/servers', {
        baseURL: API_URL_NEW
    }),
    findAllDomains: () => requestWithToken.get('plan/domain', {
        baseURL: API_URL_NEW
    }),
    checkDomain: body =>
        requestWithToken.get('plan/domain/check', {
            params: body,
            baseURL: API_URL_NEW
        }, {
            baseURL: API_URL_NEW
        }),
    buy: body => {

        return requestWithToken.post('plan/buy', getFormData({ ...body }), {
            baseURL: API_URL_NEW
        })
    }
}

export default PackageService
