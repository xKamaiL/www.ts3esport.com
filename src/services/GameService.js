import { request } from '../config/request'

const GameService = {
  fetchGames: () => request.get('/games'),
  fetchDetails: id => request.get('/games/' + id)
}

export default GameService
