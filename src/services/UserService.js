import { request, requestWithToken } from '../config/request'
import qs from 'qs'

const UserService = {
    login: (body) =>
        request.post('/auth/login', qs.stringify(body), {}),
    register: (body) => request.post('/auth/register', body),
    me: () =>
        requestWithToken.get('/user', {}),
    logout: () => requestWithToken.delete('/auth/logout'),
    profile: (body) =>
        requestWithToken.put('/auth/profile', body, {}),

    changePassword: (body) =>
        requestWithToken.put('/auth/change_password', body, {}),
    forgot: (body) =>
        request.post('/auth/forgot_password', body, {}),
    reset: (body) => request.post('/auth/reset_password', body)
}

export default UserService
