import { requestWithToken, request } from '../config/request'

const ShopService = {
  create: body => requestWithToken.post('/shop', body),
  me: () => requestWithToken.get('/shop/me'),
  update: (id, body) => requestWithToken.post('/shop/' + id + '/update', body),
  delete: id => requestWithToken.delete('/shop/' + id),
  fetchAll: params => requestWithToken.get('/shop', { params }),
  fetchDetails: id => requestWithToken.get('/shop/' + id)
}

export default ShopService
