import { API_URL_NEW, request } from '../config/request'

const ImageService = {
    getSlide: () => request.get('/image/banner', {
        baseURL: API_URL_NEW
    })
}

export default ImageService
