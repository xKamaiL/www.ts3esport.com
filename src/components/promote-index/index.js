import React, { useEffect, useState } from 'react'
import PromoteItem from './PromoteItem'
import { LoadingTopRanking } from './loading'
import PromoteService from '../../services/PromoteService'
import { Link } from 'react-router-dom'
import GameService from '../../services/GameService'
import GameItem from './GameItem'

const PromoteServerIndexPage = () => {
    const [ loading, setLoading ] = useState(true)
    const [ topRanking, setTopRanking ] = useState([])
    const [ latestRanking, setLatestRanking ] = useState([])
    const [ games, setGames ] = useState([])
    useEffect(() => {
        async function fetch() {
            await PromoteService.topTensRanking()
            .then(({ data }) => {
                setTopRanking(data)
            })
            .catch(_ => {
                setTopRanking([])
            })
            await PromoteService.latest()
            .then(({ data }) => {
                latestRanking(data)
            })
            .catch(_ => {
                setLatestRanking([])
            })
            await GameService.fetchGames()
            .then(({ data }) => {
                setGames(data)
            })
            .catch(_ => {
                setGames([])
            })
            setLoading(false)
        }

        fetch()
        document.title = 'โปรโมทเซิร์ฟเวอร์เกม'
        return () => {
        }
    }, [])
    return (
        <>
            <section className='section border-0 m-0 pb-3'>
                <div className='container container-lg'>
                    <h3>
                        Top 9 Ranking{' '}
                        <small>
                            <Link to='/promote/all'>ดูทั้งหมด</Link>
                        </small>
                    </h3>
                    <div className='row pb-1'>
                        {loading
                            ? [ ...Array(6) ].map(k => <LoadingTopRanking/>)
                            : topRanking.map(rank => <PromoteItem {...rank} />)}
                    </div>
                    <div className='d-flex justify-content-center mt-4'>
                        <Link className='btn  btn-primary mb-2'
                              to='/profile/new-promote-server'>
                            <i className='fas fa-plus'></i> โปรโมทเซิร์ฟเวอร์ของคุณ
                        </Link>
                    </div>
                </div>
            </section>
            <div className='container container-lg mt-5'>
                <h3>
                    Newest Server{' '}
                    <small>
                        <Link to='/promote/all'>ดูทั้งหมด</Link>
                    </small>
                </h3>
                <div className='row pb-1'>
                    {loading
                        ? [ ...Array(6) ].map(k => <LoadingTopRanking/>)
                        : topRanking.map(rank => <PromoteItem {...rank} />)}
                </div>
            </div>
            <section className='section bg-color-primary border-0 m-0'>
                <div className='container container-lg'>
                    <h3 className='text-center'>
                        แยกตามเกม{' '}
                        <small>
                            <Link to='/promote/all'
                                  className='text-dark'>
                                ดูทั้งหมด
                            </Link>
                        </small>
                    </h3>

                    <div className="row">
                        <div className="col-lg-4">
                            {games.map(a => (
                                <GameItem key={a.id} {...a} />
                            ))}
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default PromoteServerIndexPage
