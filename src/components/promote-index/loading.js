import React, { useEffect, useState, useContext } from 'react'
import Skeleton from 'react-skeleton-loader'
import styled from 'styled-components'
import { Link, withRouter } from 'react-router-dom'

const Server = styled.div`
  .ranking {
    background: #212529;
  }
  .banner {
  }
  .details {
    border-top: 1px solid #212529;
    border-bottom: 1px solid #212529;
    border-right: 1px solid #212529;
    box-sizing: border-box;
    .top {
    }
    .bottom {
    }
  }
  height: 160px;
`
export const LoadingTopRanking = () => {
  return (
    <div className='col-sm-6 col-lg-4 mb-4 pb-2'>
      <a>
        <article>
          <Skeleton width='100%' height={'195px'} widthRandomness='0' />
        </article>
      </a>
    </div>
  )
}

export const LoadingServerItem = () => {
  return (
    <>
      {[...Array(3)].map(_ => (
        <Server className='bg-white mb-3 d-flex flex-row justify-content-between'>
          <div className='px-0 col-1 ranking d-flex  flex-column align-items-center justify-content-start py-3'>
            <h3 className='text-white'></h3>
            <h2 className='number text-white'></h2>
          </div>
          <div className='px-0 col-4 banner'>
            <Skeleton height='160px' width='100%' widthRandomness='0' />
          </div>
          <div className='px-0 col-7 details d-flex flex-column  justify-content-between px-3'>
            <div className='top'>
              <Link to={'/promote/server/'}>
                <h2 className='mb-0'>
                  <Skeleton height='15' />
                </h2>
              </Link>
              <small className='text-primary text-3'>
                <Skeleton height='10' />
              </small>
              <p></p>
            </div>
            <div className='bottom'>
              <p>
                <Skeleton />
                <span className='text-success'></span>
              </p>
            </div>
          </div>
        </Server>
      ))}
    </>
  )
}
