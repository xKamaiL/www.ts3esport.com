import React, { useEffect, useState, useContext } from 'react'
import { Link, withRouter } from 'react-router-dom'

const GameItem = ({ name, logo_image_url, id, active, all }) => {
  return (
    <div class='px-3 py-3 rounded-0 bg-dark w-100'>
      <span class='selectgame-game-list-item-label-name'>
        <Link to={`/promote/${id}`} className='text-5'>
          {name}
        </Link>
      </span>
      <div className='ml-auto text-right'>
        <span class='text-white'>Active:</span> <span class=''>{active}</span>{' '}
        <span class='text-white'>ทั้งหมด:</span> <span class=''>{all}</span>
      </div>
    </div>
  )
}

export default GameItem
