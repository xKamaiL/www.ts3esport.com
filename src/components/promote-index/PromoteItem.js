import React, { useEffect, useState, useContext } from 'react'
import { Link, withRouter } from 'react-router-dom'

const PromoteItem = ({
  id,
  name,
  all_ranking,
  game_ranking,
  current_vote_count,
  total_latest_vote,
  game_id,
  fanpage_url,
  description,
  game,
  logo_image_url
}) => {
  return (
    <div className='col-sm-6 col-lg-4 mb-4 pb-2'>
      <Link to={`/promote/server/${id}`}>
        <article>
          <div className='thumb-info thumb-info-no-borders thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom border-radius-0'>
            <div className='thumb-info-wrapper thumb-info-wrapper-opacity-6'>
              <img
                src={logo_image_url || 'assets/img/blog/default/blog-50.jpg'}
                className='img-fluid'
                style={{ maxHeight: 195 }}
                alt={name}
              />
              <div className='thumb-info-title bg-transparent p-4'>
                <div
                  className='thumb-info-type bg-color-primary px-2 mb-1 '
                  style={{ fontWeight: 300, fontSize: 15 }}
                >
                  อันดับ {all_ranking}
                </div>
                <div className='thumb-info-inner mt-1'>
                  <h2 className='text-color-light line-height-2 text-4 font-weight-bold mb-0'>
                    {name}
                  </h2>
                </div>
                <div className='thumb-info-show-more-content'>
                  <p className='mb-0 text-1 text-white line-height-9 mb-1 mt-2 text-light opacity-5'>
                    {game?.name || `-`}{' '}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </article>
      </Link>
    </div>
  )
}

export default PromoteItem
