import React, { useEffect, useState, useContext } from 'react'
import { Link, withRouter } from 'react-router-dom'
import styled from 'styled-components'
import htmlToText from 'html-to-text'
const Server = styled.div`
  .ranking {
    background: #212529;
  }
  .banner {
  }
  .details {
    border-top: 1px solid #212529;
    border-bottom: 1px solid #212529;
    border-right: 1px solid #212529;
    box-sizing: border-box;
    .top {
    }
    .bottom {
    }
  }
  height: 160px;
`

function numberWithCommas(x) {
  x = x.toString()
  var pattern = /(-?\d+)(\d{3})/
  while (pattern.test(x)) x = x.replace(pattern, '$1,$2')
  return x
}
const ServerItem = ({
  id,
  name,
  game,
  description,
  total_latest_vote,
  logo_image_url,
  game_ranking,
  isAll,
  all_ranking
}) => {
  useEffect(() => {
    return () => {}
  }, [])
  return (
    <Server className='bg-white mb-3 d-flex flex-row justify-content-between'>
      <div className='px-0 col-1 ranking d-flex  flex-column align-items-center justify-content-start py-3'>
        <h3 className='text-white'>อันดับ</h3>
        <h2 className='number text-white'>
          {isAll ? all_ranking : game_ranking}
        </h2>
      </div>
      <div className='px-0 col-4 banner'>
        <img
          src={logo_image_url || '/assets/img/blog/wide/blog-11.jpg'}
          height={160}
          width='100%'
          alt=''
        />
      </div>
      <div className='px-0 col-7 details d-flex flex-column  justify-content-between px-3'>
        <div className='top'>
          <Link to={'/promote/server/' + id}>
            <h2 className='mb-0'>{name}</h2>
          </Link>
          <small className='text-primary text-3'>{game.name}</small>
          <p>
            {htmlToText.fromString(description, {
              wordwrap: 130
            })}
          </p>
        </div>
        <div className='bottom'>
          <p>
            Total Vote{' '}
            <span className='text-success'>
              {numberWithCommas(total_latest_vote)}
            </span>
          </p>
        </div>
      </div>
    </Server>
  )
}

export default ServerItem
