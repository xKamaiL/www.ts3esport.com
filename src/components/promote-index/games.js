import React, { useEffect, useState, useContext } from 'react'
import { Link, withRouter } from 'react-router-dom'
import PromoteService from '../../services/PromoteService'
import GameService from '../../services/GameService'
import ServerItem from './ServerItem'
import CustomPagination from '../ui/CustomPaginate'
import { LoadingServerItem } from './loading'

const PromoteServerIndexGames = ({ match: { params }, history }) => {
  const [loading, setLoading] = useState(true)
  const [servers, setServers] = useState({
    totalItemsOnPage: 0,
    totalItems: 0,
    currentPage: 1,
    pageSize: 25,
    totalPage: 1,
    firstPage: true,
    lastPage: true,
    isNext: false,
    items: []
  })
  const [page, setPage] = useState(1)
  const [gameData, setGameData] = useState(null)
  const game_id = params.game
  const fetchData = async () => {
    if (game_id !== 'all') {
      await GameService.fetchDetails(game_id)
        .then(({ data }) => {
          setGameData(data)
          document.title = `Top Ranking ${data.name}`
        })
        .catch(_ => {
          history.push('/not-found')
        })
    } else {
      setGameData({
        name: 'ทั้งหมด'
      })
    }
    await PromoteService.findPromoteByGameId(game_id, {
      page
    })
      .then(({ data }) => {
        setServers(data)
      })
      .catch(error => {
        setServers({
          ...servers,
          items: []
        })
      })
    setLoading(false)
  }
  useEffect(() => {
    fetchData()
    return () => {}
  }, [])
  return (
    <div className='main'>
      <section class='page-header page-header-modern bg-color-light-scale-1 page-header-sm'>
        <div class='container'>
          <div class='row'>
            <div class='col-md-8 order-2 order-md-1 align-self-center p-static'>
              {!loading && (
                <h1 class='text-dark'>
                  Top Ranking <strong>{gameData.name}</strong>
                </h1>
              )}
            </div>

            <div class='col-md-4 order-1 order-md-2 align-self-center'>
              <ul class='breadcrumb d-block text-md-right'>
                <li>
                  <Link to='/promote'>ย้อนกลับ</Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <div className='container container-lg'>
        {!loading &&
          servers.items.map(s => (
            <ServerItem isAll={game_id === 'all'} {...s} key={s.id} />
          ))}
        {loading && <LoadingServerItem />}
        <div className='col-12'>
          <CustomPagination
            {...servers}
            onChange={page => {
              setPage(page)
              fetchData()
            }}
          />
        </div>
      </div>
    </div>
  )
}

export default withRouter(PromoteServerIndexGames)
