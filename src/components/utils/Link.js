import React from 'react';
import { routingStore } from '../common/App';

const Link = ({ children, path }) => (
  <React.Fragment>
    <a
      href={path}
      onClick={event => {
        event.preventDefault();
        routingStore.push(path);
      }}
    >
      {children}
    </a>
  </React.Fragment>
);
export default Link;
