import React from 'react';
import { NotificationManager } from 'react-notifications';

const handleError = error_response => {
  const {
    response: {
      status,
      data: { message, errors, title }
    }
  } = error_response;
  const errorMessage = [];
  switch (status) {
    case 422:
      return Object.keys(errors).map(err => {
        return NotificationManager.warning(
          `${errors[err].map(a => a)}`,
          message
        );
      });
    case 200:
      return NotificationManager.success(message, title);
    case 500:
      return NotificationManager.error(message, title);
    default:
      return NotificationManager.warning(message, title);
  }
};
export default handleError;
