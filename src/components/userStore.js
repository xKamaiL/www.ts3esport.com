import { action, observable } from 'mobx'
import UserService from '../services/UserService'
import { NotificationManager } from 'react-notifications'

class Store {
    @observable
    isLogin = false
    @action
    setStatusLogout = () => {
    }
    @observable login_data = {
        username: '',
        password: ''
    }
    @observable register_data = {
        username: '',
        password: '',
        password_confirmation: '',
        email: '',
        pincode: '',
        tel: '',
        token: ''
    }
    @observable login_modal = false
    @observable userData = null
    @observable firstLoadApp = true

    @action
    async getProfile() {
        await UserService.me()
        .then(({ data }) => {
            this.userData = data
            this.isLogin = true
        })
        .catch(() => {
            this.userData = null
            this.isLogin = false
        })
        this.firstLoadApp = false
    }

    @action
    submitLogin = () => {
        return UserService.login(this.login_data)
        .then(({ data }) => {
            localStorage.setItem('accessToken', data.token)
            this.getProfile().then(() => {
                NotificationManager.success('', 'ยินดีต้อนรับ')
            })
        })
        .catch((_) => {
        })
    }
    @action
    registerUser = () => {
        return UserService.register(this.register_data)
        .then(({ data }) => {
            localStorage.setItem('accessToken', data.token)
            this.getProfile().then(() => {
                window.location.href = '/'
            })
        })
        .catch((_) => {
        })
    }
    @action
    setStatusLogout = () => {
        this.isLogin = false
    }
    @action
    logout = (event) => {
        event.preventDefault()
        this.isLogin = false
        UserService.logout().then((_) => {
            this.userData = null
            localStorage.clear()
            window.location.reload()
        })
    }
}

const userStore = new Store()
export default userStore
