import React, { useEffect, useState, useContext } from 'react'
import { Link, withRouter } from 'react-router-dom'

const Search = ({ onChange, value }) => {
  return (
    <React.Fragment>
      <Link
        className='btn btn-block btn-primary mb-2'
        to='/profile/new-shop-item'
      >
        <i className='fas fa-plus'></i> ลงขายไอดีเกม
      </Link>
      <form action='#' onSubmit={e => e.preventDefault()} method='get'>
        <div class='input-group mb-3 pb-1'>
          <input
            class='form-control text-1'
            placeholder='ค้นหาสินค้า...'
            name='search'
            type='text'
            value={value}
            onChange={e => onChange(e.target.value)}
          />
          <span class='input-group-append'>
            <button type='submit' class='btn btn-dark text-1 p-2'>
              <i class='fas fa-search m-2'></i>
            </button>
          </span>
        </div>
      </form>
    </React.Fragment>
  )
}

export default Search
