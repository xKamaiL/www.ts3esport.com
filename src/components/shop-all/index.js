import React, { useEffect, useState, useContext } from 'react'
import Search from './Search'
import GamesFilter from './GamesFilter'
import ShopItem from './ShopItem'
import ShopService from '../../services/ShopService'
import CustomPagination from '../ui/CustomPaginate'

const ShopAllPage = () => {
  const [loading, setLoading] = useState(true)
  const [gameId, setGameId] = useState(null)
  const [page, setPage] = useState(1)
  const [search, setSearch] = useState(null)
  const [shopItem, setShopItem] = useState({
    items: []
  })
  const onSearch = value => {
    setSearch(value)
  }
  const onChangeGame = game_id => {
    setGameId(game_id)
  }
  useEffect(() => {
    document.title = 'ซื้อขายไอดีเกม'
    return () => {}
  }, [])
  useEffect(() => {
    setLoading(true)
    ShopService.fetchAll({
      page,
      search,
      game_id: gameId
    })
      .then(re => re.data)
      .then(data => {
        setShopItem(data)
        setLoading(false)
      })
      .catch(error => {
        alert('เกิดข้อผิดพลาด')
        setShopItem({
          ...shopItem,
          items: []
        })
        setLoading(false)
      })

    return () => {}
  }, [gameId, search, page])
  return (
    <div className='main shop py-4'>
      <div className='container'>
        <div className='row'>
          <div className='col-lg-3'>
            <aside className='sidebar'>
              <Search onChange={onSearch} value={search} />
              <GamesFilter onChange={onChangeGame} value={gameId} />
            </aside>
          </div>
          <div className='col-lg-9'>
            <div className='masonry-loader masonry-loader-loaded'>
              <div
                className='row products product-thumb-info-list'
                style={{
                  height:
                    !loading && shopItem.items.length === 0 ? 350 : 'unset'
                }}
              >
                {!loading &&
                  shopItem.items.map(shop => (
                    <ShopItem key={shop.id} {...shop} />
                  ))}
                {loading === false && shopItem.items.length === 0 && (
                  <p className='mt-5 mx-auto pt-5'>ไม่มีสินค้า</p>
                )}
                {loading && <p className='mt-5 mx-auto pt-5'>กำลังโหลด...</p>}
              </div>
              <CustomPagination
                {...shopItem}
                onChange={page => {
                  setPage(1)
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ShopAllPage
