import React, { useEffect, useState, useContext } from 'react'
import { Link, withRouter } from 'react-router-dom'

const ShopItem = ({ id, name, logo_image_url, user, price, game }) => {
  return (
    <div class='col-sm-6 col-lg-4 product'>
      <span class='product-thumb-info border-0'>
        <a to={`/shop/${id}`} class='add-to-cart-product bg-color-primary'>
          <span class='text-uppercase text-1'>{game?.name|| `-`}</span>
        </a>
        <Link to={`/shop/${id}`}>
          <span class='product-thumb-info-image'>
            <img
              alt=''
              height='235'
              src={logo_image_url || '/assets/img/products/product-grey-1.jpg'}
            />
          </span>
        </Link>
        <span class='product-thumb-info-content product-thumb-info-content pl-0 bg-color-light'>
          <Link to={`/shop/${id}`}>
            <h4 class='text-4 text-primary'>{name}</h4>
            <span class='price'>
              <ins>
                <span class='amount text-dark font-weight-semibold'>
                  {price} ฿
                </span>
              </ins>
            </span>
          </Link>
        </span>
      </span>
    </div>
  )
}

export default ShopItem
