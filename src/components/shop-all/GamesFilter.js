import React, { useEffect, useState, useContext } from 'react'
import GameService from '../../services/GameService'
const GamesFilter = ({ onChange, value }) => {
  const [loading, setLoading] = useState(true)
  const [games, setGames] = useState([])
  useEffect(() => {
    async function fetch() {
      await GameService.fetchGames()
        .then(res => res.data)
        .then(data => {
          setGames(data)
        })
        .catch(erro => {
          setGames([])
        })
      setLoading(false)
    }
    fetch()
    return () => {}
  }, [])
  return (
    <React.Fragment>
      <h5 class='font-weight-bold pt-3'>Games</h5>
      <ul class='nav nav-list flex-column'>
        {games.map(a => (
          <li class='nav-item '>
            <a
              class={`nav-link ${value === a.id ? 'active' : null}`}
              href=''
              onClick={e => {
                e.preventDefault()
                if (value !== a.id) {
                  onChange(a.id)
                } else {
                  onChange(null)
                }
              }}
            >
              {a.name}
            </a>
          </li>
        ))}
      </ul>
    </React.Fragment>
  )
}

export default GamesFilter
