import React, { useEffect, useState } from 'react'

const PageInternalError = () => {
  return (
    <section class="http-error py-0">
      <div class="row justify-content-center py-3">
        <div class="col-6 text-center">
          <div class="http-error-main">
            <h2 class="mb-0">500!</h2>
            <span class="text-6 font-weight-bold text-color-dark">
              UNEXPECTED ERROR
            </span>
            <p class="text-3 my-4">
              An unexpected error has occured. Lorem ipsum dolor sit amet,
              consectetur adipiscing elit. Phasellus blandit massa enim.
            </p>
          </div>
          <a
            href="index.html"
            class="btn btn-primary btn-rounded btn-xl font-weight-semibold text-2 px-4 py-3 mt-1 mb-4"
          >
            <i class="fas fa-angle-left pr-3" />GO BACK TO HOME PAGE
          </a>
        </div>
      </div>
    </section>
  )
}

export default PageInternalError
