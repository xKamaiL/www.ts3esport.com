import React, { useEffect, useState } from 'react'

const NoAuthenticate = () => {
  return (
    <div className="container">
      <div className="row mt-5">
        <div className="col text-center">
          <div className="logo">
            <a href="index.html">
              <img
                width={100}
                height={48}
                src="/assets/img/logo.png"
                alt="Porto"
              />
            </a>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <hr className="solid my-5" />
        </div>
      </div>
      <div className="row mt-5 mb-5">
        <div className="col text-center">
          <div className="overflow-hidden mb-2">
            <h2
              className="font-weight-normal text-7 mb-0 appear-animation animated maskUp appear-animation-visible"
              data-appear-animation="maskUp"
              style={{ animationDelay: '100ms' }}
            >
              <strong className="font-weight-extra-bold">
                คุณไม่มีสิทธิ๋ในการเข้าถึงหน้าดังกล่าว
              </strong>
            </h2>
          </div>
          <div className="overflow-hidden mb-1">
            <p
              className="lead mb-0 appear-animation animated maskUp appear-animation-visible"
              data-appear-animation="maskUp"
              data-appear-animation-delay={200}
              style={{ animationDelay: '200ms' }}
            >
              กรุณาเข้าสู่ระบบก่อนทำรายการ
              <br />
            </p>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <hr className="solid my-5" />
        </div>
      </div>
    </div>
  )
}

export default NoAuthenticate
