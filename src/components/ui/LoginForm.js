import React, { useEffect, useState } from 'react'
import userStore from '../userStore'
import { Observer } from 'mobx-react'
import { Link, withRouter } from 'react-router-dom'

const LoginForm = ({ setcurrentForm, history }) => {
    const [loading, setLoading] = useState(false)
    const submit = async event => {
        event.preventDefault()
        setLoading(true)
        await userStore.submitLogin()
        setLoading(false)
    }
    const handleInput = ({ target: { name, value } }) => {
        userStore.login_data[name] = value
    }
    useEffect(() => {
    }, [history])

    return (
        <Observer>
            {() => (
                <form onSubmit={submit}>
                    <div className='form-group'>
                        <label className='mb-1 text-2 opacity-8'>ชื่อผู้ใช้ * </label>
                        <input
                            type='text'
                            className='form-control form-control-lg'
                            placeholder='Username'
                            name='username'
                            value={userStore.login_data.username}
                            onChange={handleInput}
                            disabled={loading}
                        />
                    </div>
                    <div className='form-group'>
                        <label className='mb-1 text-2 opacity-8'>รหัสผ่าน *</label>
                        <input
                            type='password'
                            name='password'
                            className='form-control form-control-lg'
                            placeholder='Password'
                            value={userStore.login_data.password}
                            onChange={handleInput}
                            disabled={loading}
                        />
                    </div>

                    <div className='actions'>
                        <div className='form-row'>
                            <div className='col d-flex justify-content-between align-items-center'>
                                <Link
                                    className='text-uppercase text-1 font-weight-bold text-color-dark'
                                    to='/forgot-password'
                                >
                                    ลืมรหัสผ่าน ?
                                </Link>
                                <button
                                    type='submit'
                                    className='btn btn-primary'
                                    disabled={loading}
                                >
                                    เข้าสู่ระบบ
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className='extra-actions'>
                        <p style={{ fontSize: '14px' }}>
                            ยังไม่มีบัญชีใช่หรือไม่{' '}
                            <Link
                                to='/register'
                                className='text-uppercase text-1 font-weight-bold text-color-dark'
                                onClick={e => {
                                    userStore.login_modal = false
                                }}
                            >
                                สมัครเลย
                            </Link>
                        </p>
                    </div>
                </form>
            )}
        </Observer>
    )
}

export default withRouter(LoginForm)
