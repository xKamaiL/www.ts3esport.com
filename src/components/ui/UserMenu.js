import React, { useEffect, useState } from 'react'
import { Observer, observer } from 'mobx-react'
import userStore from '../userStore'
import { withRouter, Link } from 'react-router-dom'
const UserMenu = () => {
  const user = userStore.userData
  if (!userStore.isLogin) return null
  return (
    <Observer>
      {() => (
        <li class='dropdown'>
          <a class='dropdown-item' href='#'>
            <i className='fas fa-user mr-2' />
            {user.username} ({user.point} $)
          </a>
          <ul class='dropdown-menu'>
            <li>
              <Link to='/profile' class='dropdown-item'>
                แก้ไขโปรไฟล์
              </Link>
            </li>
            <li>
              <a
                class='dropdown-item'
                href='/logout'
                onClick={userStore.logout}
              >
                ออกจากระบบ
              </a>
            </li>
          </ul>
        </li>
      )}
    </Observer>
  )
}

export default observer(UserMenu)
