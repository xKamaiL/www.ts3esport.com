import React, { useEffect, useState, Fragment } from 'react'

const DownloadTable = () => {
  useEffect(() => {
    document.title = 'Downloads'
    return () => {}
  }, [])
  return (
    <Fragment>
      <section class="page-header page-header-classic">
        <div class="container">
          <div class="row">
            <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
              <h1>Download</h1>
            </div>
          </div>
        </div>
      </section>
      <div className="container">
        <table class="table table-striped">
          <tbody>
            <tr>
              <th className="text-center w-50 ">
                <span>TeamSpeak3-Client-win32-3.1.10</span>
              </th>
              <td className="text-center">
                <a
                  href="https://storage.googleapis.com/laravel-ts3esport/TeamSpeak3-Client-win32-3.1.10.exe"
                  target="_blank"
                  className="btn btn-primary w-75"
                >
                  Download
                </a>
              </td>
            </tr>
            <tr>
              <th className="text-center w-50 ">
                TeamSpeak3-Client-win64-3.1.10
              </th>
              <td className="text-center">
                <a
                  href="https://storage.googleapis.com/laravel-ts3esport/TeamSpeak3-Client-win64-3.1.10.exe"
                  target="_blank"
                  className="btn btn-primary w-75"
                >
                  Download
                </a>
              </td>
            </tr>
            <tr>
              <th className="text-center w-50 ">YaTQA</th>
              <td className="text-center">
                <a
                  href="https://www.ts3esport.com/YaTQA-Setup.exe"
                  target="_blank"
                  className="btn btn-primary w-75"
                >
                  Download
                </a>
              </td>
            </tr>
            <tr>
              <th className="text-center w-50 ">AnyDesk</th>
              <td className="text-center">
                <a
                  href="https://www.ts3esport.com/AnyDesk.exe"
                  target="_blank"
                  className="btn btn-primary w-75"
                >
                  Download
                </a>
              </td>
            </tr>
            <tr>
              <th className="text-center w-50 ">ICON</th>
              <td className="text-center">
                <a
                  href="https://storage.googleapis.com/laravel-ts3esport/icon.rar"
                  target="_blank"
                  className="btn btn-primary w-75"
                >
                  Download
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </Fragment>
  )
}

export default DownloadTable
