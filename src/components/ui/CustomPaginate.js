import React, { useEffect, useState } from 'react'
import ReactJsPagination from 'react-js-pagination'
import styled from 'styled-components'
const Wrapper = styled.div`
  ul.pagination {
    li {
      a {
        font-size: 18px;
        padding: 5px 24px;
      }
      .active {
      }
    }
    li.disabled {
      a {
        pointer-events: none;
      }
    }
  }
`
const CustomPagination = ({
  onChange,
  currentPage,
  totalItems,
  firstPage,
  isNext,
  totalPage,
  pageSize,
  totalItemsOnPage,
  lastPage,
  className
}) => {
  return (
    <Wrapper className='float-right mt-3'>
      <ReactJsPagination
        activePage={currentPage}
        itemCountPerPage={totalItemsOnPage}
        totalItemsCount={totalPage}
        pageRangeDisplayed={10}
        onChange={onChange}
        linkClass='btn square text-white'
        innerClass={className}
      />
    </Wrapper>
  )
}

export default CustomPagination
