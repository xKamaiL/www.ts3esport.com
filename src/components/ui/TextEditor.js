import React, { Component } from 'react'
import { EditorState, convertToRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import PropTypes from 'prop-types'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
const ToolbarOptions = {
  options: [
    'inline',
    'blockType',
    'list',
    'textAlign',
    'link',
    'emoji',
    'image',
    'history'
  ],
  inline: {
    options: ['bold', 'italic', 'underline']
  }
}

export default class TextEditor extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired
  }

  constructor() {
    super()

    this.onEditorStateChange = this.onEditorStateChange.bind(this)
  }

  onEditorStateChange = editorState => {
    const { onChange } = this.props

    if (onChange) {
      onChange(editorState)
    }
  }

  render() {
    const { editorState } = this.props
    return (
      <div className='text-editor' style={{ border: '1px solid darkgrey' }}>
        <Editor
          editorState={editorState}
          wrapperClassName='demo-wrapper'
          editorClassName='demo-editor'
          onEditorStateChange={this.onEditorStateChange}
          toolbar={ToolbarOptions}
        />
      </div>
    )
  }
}
