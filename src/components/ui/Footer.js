import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return (
            <footer id="footer">
                <div className="footer-copyright">
                    <div className="container py-2">
                        <div className="row py-4">
                            <div className="col-lg-1 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0">
                                <a href="index.html"
                                   className="logo pr-0 pr-lg-3"/>
                            </div>
                            <div className="col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-start mb-4 mb-lg-0">
                                <p>
                                    © Copyright 2019. All Rights Reserved. Powered by{' '}
                                    <small>
                                        <a rel="noreferrer"
                                           href="https://www.facebook.com/xKamaiL"
                                           target="_blank">
                                            xKamaiL
                                        </a>
                                    </small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer
