import React, { useEffect, useState, useContext } from 'react'
import UserService from '../../services/UserService'
const ForgotPassword = () => {
  const [username, setUsername] = useState('')
  const [success, setSuccess] = useState(false)
  const [l, setL] = useState(false)
  const submit = async e => {
    e.preventDefault()
    setL(true)
    await UserService.forgot({
      username
    })
      .then(({ data }) => {
        setSuccess(data.email)
      })
      .catch(err => {})
    setL(false)
  }
  return (
    <div class='row justify-content-md-center'>
      <div class='col-md-6 mt-5'>
        <div class='featured-box featured-box-primary text-left mt-0'>
          <div class='box-content'>
            <h4 class='color-primary font-weight-semibold text-4 text-uppercase mb-0'>
              ลืมรหัสผ่าน ?
            </h4>
            <p class='text-2 opacity-7'>
              กรุณากรอก ยูสเซอร์เนมข้างล่าง
              ระบบจะส่งลิ้งค์รีเซ็ตรหัสผ่านไปยังอีเมล์ของคุณ
              
            </p>
            {success ? (
              <div className='text-center'>
                <h1>
                  <i className='fas fa-check text-success'> </i>
                </h1>
                <h3>
                  ระบบได้ทำการส่งลิ้งค์เปลียนแปลงรหัสผ่านไปยังอีเมล์ของคุณแล้ว (กรุณาเช็คในอีเมล์ขยะ หากไม่พบอีเมล์ดังกล่าว)
                </h3>
                <p>{success}</p>
              </div>
            ) : (
              <form onSubmit={submit}>
                <div class='form-row'>
                  <div class='form-group col'>
                    <label class='font-weight-bold text-dark text-2'>
                      ชื่อผู้ใช้งาน
                    </label>
                    <input
                      type='text'
                      value={username}
                      onChange={({ target: { value } }) => {
                        setUsername(value)
                      }}
                      class='form-control form-control-lg'
                    />
                  </div>
                </div>
                <div class='form-row'>
                  <div class='form-group col'>
                    <button
                      type='submit'
                      class='btn btn-primary btn-modern float-right'
                    >
                      ยืนยัน
                    </button>
                  </div>
                </div>
              </form>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default ForgotPassword
