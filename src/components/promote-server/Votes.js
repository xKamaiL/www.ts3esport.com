import React, { useEffect, useState, useContext } from 'react'
import { observer } from 'mobx-react-lite'
import userStore from '../userStore'
import ReCAPTCHA from 'react-google-recaptcha'
import { NotificationManager } from 'react-notifications'
import { withRouter, Link } from 'react-router-dom'
import PromoteService from '../../services/PromoteService'

const Votes = ({ server_id }) => {
  const [recaptchaInstance, setrecaptchaInstance] = useState(null)
  const [loading, setLoading] = useState(false)
  const [captcha, setcaptcha] = useState(false)
  const [username, setUsername] = useState('')
  const [token, setToken] = useState(null)

  const submit = async event => {
    event.preventDefault()
    setLoading(true)
    if (!_.isNull(token)) {
      NotificationManager.error('', 'กรุณายืนยันตัวตน')
    } else {
      await PromoteService.voteServer(server_id, {
        id_game_reference: username,
        token
      })
        .then(res => res.data)
        .then(data => {
          NotificationManager.success(
            'กรุณารอ 60 วินาทีเพื่อทำการโหวตใหม่',
            'โหวตให้เรียบร้อยแล้ว'
          )
        })
        .catch(error => {})
    }
    recaptchaInstance.reset()
    setLoading(false)
  }
  useEffect(() => {}, [])
  const verifyCallback = e => {
    if (loading === false) {
      setToken(token)
    }
  }
  if (!userStore.isLogin) {
    return null
  }
  return (
    <form action='' onSubmit={submit}>
      <div className='d-flex justify-content-center flex-column align-items-center'>
        <ReCAPTCHA
          sitekey='6Lei5J4UAAAAANPbB-wt76CXNghtbOfeGOi0wESO'
          onChange={verifyCallback}
        />
        <div className='form-group mt-4 w-50'>
          <input
            type='text'
            placeholder='ไอดีเกม (ถ้ามี)'
            className='form-control '
            onChange={({ target: { value } }) => {
              setUsername(value)
            }}
            disabled={loading}
            value={username}
          />
        </div>
        <button
          disabled={loading}
          className='submit'
          className='btn btn-success  btn-lg w-50'
        >
          ยืนยันโหวต
        </button>
      </div>
    </form>
  )
}

export default observer(Votes)
