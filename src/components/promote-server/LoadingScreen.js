import React, { useEffect, useState, useContext } from 'react'
import Skeleton from 'react-skeleton-loader'

const LoadingScreen = () => {
  return (
    <div className='main'>
      <div className='container container py-4'>
        <div className='row'>
          <div className='col'>
            <div className='blog-posts single-post'>
              <article className='post post-large blog-single-post border-0 m-0 p-0'>
                <div class='post-date ml-0 mr-0'>
                  <span class='day'>0</span>
                  <span class='month'>
                    <Skeleton width='45px' widthRandomness='0' />
                  </span>
                </div>
                <div class='post-date ml-0 '>
                  <span class='day text-danger'>0</span>
                  <span class='month bg-danger'>
                    <Skeleton width='45px' widthRandomness='0' />
                  </span>
                </div>
                <div className='post-content ml-0'>
                  <h2 class='font-weight-bold'>
                    <a href='blog-post.html'>
                      <Skeleton />
                    </a>
                  </h2>
                  <div class='post-meta'>
                    <span>
                      <i class='fab fa-facebook-square'></i> <Skeleton />
                    </span>
                    <span>
                      <i class='fab fa-facebook-square'></i> <Skeleton />
                    </span>
                    <span>
                      <i class='fas fa-search'></i> <Skeleton />
                    </span>
                  </div>
                  <p>
                    <Skeleton count={15} width='100%' />
                  </p>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default LoadingScreen
