import React from 'react'

const BannerImage = ({ logo_image_url }) => {
    return (
        <img
            src={logo_image_url.replace('https://api.ts.in.th', process.env.REACT_APP_API_URL) || '/assets/img/blog/medium/blog-5.jpg'}
            class='img-fluid float-left mr-4 mt-2'
            alt={logo_image_url}/>
    )
}

export default BannerImage
