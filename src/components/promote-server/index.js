import React, { useEffect, useState, useContext } from 'react'
import PromoteService from '../../services/PromoteService'
import BannerImage from './Banner'
import ServerTitle from './Title'
import LoadingScreen from './LoadingScreen'
import Votes from './Votes'
const PromoteServerDetails = ({
  match: {
    params: { server_id }
  }
}) => {
  const [loading, setLoading] = useState(true)
  const [serverData, setServerData] = useState(null)
  const [error, setError] = useState(false)
  useEffect(() => {
    async function fetchData() {
      await PromoteService.fetchDetails(server_id)
        .then(res => res.data)
        .then(data => {
          setServerData(data)
          setError(false)
          document.title = data.name
        })
        .catch(error => {
          if (error.response.status == 404) {
            history.push('/not-found')
          }
          setError(true)
        })
      setLoading(false)
    }
    fetchData()
    document.title = ''
    return () => {}
  }, [])
  if (loading) {
    return <LoadingScreen />
  }
  return (
    <div className='main'>
      <div className='container container py-4'>
        <div className='row'>
          <div className='col'>
            <div className='blog-posts single-post'>
              <article className='post post-large blog-single-post border-0 m-0 p-0'>
                <div class='post-date ml-0 mr-0'>
                  <span class='day'>{serverData.game_ranking}</span>
                  <span class='month'>GAMES</span>
                </div>
                <div class='post-date ml-0 '>
                  <span class='day text-danger'>{serverData.all_ranking}</span>
                  <span class='month bg-danger'>GLOBAL</span>
                </div>
                <div className='post-content ml-0'>
                  <ServerTitle {...serverData} />
                  <BannerImage {...serverData} />
                  <div
                    className='content'
                    dangerouslySetInnerHTML={{
                      __html: serverData.description
                    }}
                  ></div>
                </div>
              </article>
            </div>
          </div>
        </div>
        <div className='row'>
          <div className='col'>
            <div class='divider'>
              <i class='fas fa-chevron-down'></i>
            </div>
            <div class='post-block mt-5 post-share text-center'>
              <h3 class='mb-3'>Vote ให้เซิฟเวอร์นี้</h3>
              <Votes server_id={serverData.id} />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PromoteServerDetails
