import React, { useEffect, useState, useContext } from 'react'
const ServerTitle = ({
  name,
  all_ranking,
  game_ranking,
  created_at,
  website_url,
  fanpage_url,
  fb_group_url,
  total_latest_vote
}) => {
  return (
    <>
      <h2 class='font-weight-bold'>
        <a href=''>{name}</a>{' '}
        <small className='float-right'>
          จำนวนโหวตทั้งหมด {total_latest_vote} โหวต
        </small>
      </h2>
      <div class='post-meta'>
        <span>
          <i class='fab fa-facebook-square'></i>{' '}
          {_.isNull(fanpage_url) ? (
            'ไม่มีแฟนเพจ'
          ) : (
            <a target='_blank' href={fanpage_url}>
              แฟนเพจ
            </a>
          )}
        </span>
        <span>
          <i class='fab fa-facebook-square'></i>{' '}
          {_.isNull(fb_group_url) ? (
            'ไม่มีกลุ่มเฟส'
          ) : (
            <a target='_blank' href={fb_group_url}>
              กลุ่มเฟส
            </a>
          )}
        </span>
        <span>
          <i class='fas fa-search'></i>{' '}
          {_.isNull(website_url) ? (
            'ไม่มีเว็บไซต์'
          ) : (
            <a target='_blank' href={website_url}>
              {website_url}
            </a>
          )}
        </span>
      </div>
    </>
  )
}

export default ServerTitle
