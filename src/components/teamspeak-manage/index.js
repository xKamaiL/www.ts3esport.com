import React, { useEffect, useState } from 'react'
import store from './store'
import Sidebar from './Sidebar'

import { withRouter, Link } from 'react-router-dom'
import Loading from './Loading'
import DashbaordTab from './DashbaordTab'
import PrivilegeTab from './PrivilegeTab'
import { Observer, observer } from 'mobx-react'
import Skeleton from 'react-skeleton-loader'
import { routingStore } from '../common/App'
import RenewTab from './RenewTab'
import ChangeAddressTab from './ChangeAddressTab'
import ClientTab from './ClientTab'
import CancelTab from './CancelTab'

export const PageHeader = observer(({ children }) => (
  <section class="page-header page-header-classic">
    <div class="container">
      <div class="row">
        <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
          <h1 data-title-border>Server Manage</h1>
        </div>
        <div class="col-md-4 order-1 order-md-2 align-self-center">
          <ul class="breadcrumb d-block text-md-right">
            <li>
              <Link to="/teamspeak/list">My Server</Link>
            </li>
            <li class="active">{store.data.domain}</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
))

const SwitcherControl = ({ tab, is_expire = false }) => {
  let content
  switch (tab) {
    case 'home':
      content = <DashbaordTab />
      break
    case 'privilege':
      if (is_expire) return <RenewTab />
      content = <PrivilegeTab />
      break
    case 'cancel':
      content = <CancelTab />
      break
    case 'change_address':
      if (is_expire) return <RenewTab />
      content = <ChangeAddressTab />
      break
    case 'clients':
      if (is_expire) return <RenewTab />
      content = <ClientTab />
      break
    case 'renew':
      content = <RenewTab />
      break
    default:
      window.location.href = '/not-found'
      content = (
        <div>
          <p>Not Found </p>
        </div>
      )
      break
  }
  return (
    <div class="featured-box featured-box-primary featured-box-text-left mt-0">
      <div class="box-content p-4">{content}</div>
    </div>
  )
}

const TeamspeakManage = ({
  match: {
    params: { id, tab }
  },
  history
}) => {
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    store.id = id
    store
      .fetchDetail()
      .then(() => {
        setLoading(false)
      })
      .catch(e => {
        console.log(e)
        history.push('/not-found')
      })

    return () => {}
  }, [])
  if (loading) return <Loading id={id} tab={tab} />
  return (
    <React.Fragment>
      <PageHeader />
      <div className="container">
        {!loading && store?.data?.is_expire && (
          <div className="alert alert-secondary">
            <strong>คำเตือน!</strong> เซิฟเวอร์ของคุณหมดอายุ มาแล้ว{' '}
            {store.data.expire_left} วัน หากเกิน 3
            วันระบบจะลบเซิฟเวอร์ของคุณทันที{' '}
            <Link to={'/teamspeak/manage/' + id + '/renew'} class="alert-link">
              ต่ออายุ
            </Link>
            .
          </div>
        )}
        <div className="row">
          <Sidebar
            id={id}
            tab={tab}
            is_expire={!loading && store.data?.is_expire}
          />
          <div className="col-md-12 col-lg-8">
            <SwitcherControl
              tab={tab}
              is_expire={!loading && store.data?.is_expire}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default withRouter(TeamspeakManage)
