import React, { useEffect, useState } from 'react'
import { withRouter, Link } from 'react-router-dom'
import Skeleton from 'react-skeleton-loader'
import Sidebar from './Sidebar'

const Loading = ({ id, tab }) => {
  return (
    <React.Fragment>
      <section class="page-header page-header-classic">
        <div class="container">
          <div class="row">
            <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
              <h1 data-title-border>Server Manage</h1>
            </div>
            <div class="col-md-4 order-1 order-md-2 align-self-center">
              <ul class="breadcrumb d-block text-md-right">
                <li>
                  <Link to="/teamspeak/list">My Server</Link>
                </li>
                <li class="active">
                  <Skeleton />
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <div className="container">
        <div className="row">
          <Sidebar id={id} tab={tab} />
          <div className="col-md-8">
            <div class="featured-box featured-box-primary featured-box-text-left mt-0">
              <div class="box-content p-4">
                <Skeleton width="100%" count={10} height="20px" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Loading
