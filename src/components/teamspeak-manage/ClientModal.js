import React, { useState, useEffect } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import ServerService from '../../services/ServerService'
import store from './store'

function ClientModal({ toggle, title, children }) {
  const [clients, setClients] = useState([])
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    const { id } = store.data
    const loading = new Loading()
    ServerService.manage
      .get('server/' + id + '/clients')
      .then(({ data }) => {
        setClients(data)
        setLoading(false)
        loading.out()
      })
      .catch(_ => {
        setClients([])
        setLoading(false)
        loading.out()
      })
    return () => {}
  }, [open])
  if (loading) return null
  return (
    <Modal isOpen={true} toggle={toggle}>
      <ModalHeader toggle={toggle}>{title}</ModalHeader>
      <ModalBody>{children(clients)}</ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={toggle}>
          ปิด
        </Button>
      </ModalFooter>
    </Modal>
  )
}

export default ClientModal
