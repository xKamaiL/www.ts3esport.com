import React, { useEffect, useState } from 'react'
import ServerService from '../../services/ServerService'
import store from './store'
import { NotificationManager } from 'react-notifications'

const ClientTab = () => {
    const [clients, setClients] = useState([])
    const [bans, setBans] = useState([])

    function fetchBans() {
        ServerService.manage
            .get('server/' + store.data?.id + '/clients/ban')
            .then(({ data }) => {
                setBans(data)
            })
            .catch(_ => {
                setBans([])
            })
    }

    function fetchClients() {
        ServerService.manage
            .get('server/' + store.data?.id + '/clients')
            .then(({ data }) => {
                setClients(data)
            })
            .catch(_ => {
                setClients([])
            })
    }

    useEffect(() => {
        fetchClients()
        fetchBans()
        return () => {
        }
    }, [])
    useEffect(() => {
        let a = setInterval(() => {
            fetchClients()
            fetchBans()
        }, 5000)
        return () => {
            clearInterval(a)
        }
    }, [])

    async function unbanClient(id) {
        ServerService.manage
            .delete('server/' + store.data?.id + '/clients/ban', {
                params: {
                    client_id: id
                }
            })
            .then(({ data }) => {
                NotificationManager.success('', data.message)
                fetchBans()
            })
            .catch(_ => {
            })
    }

    async function BanClientById(client_id) {
        if (!confirm('คุณแน่ใจ ?')) return false
        ServerService.manage
            .post('server/' + store.data?.id + '/clients/ban', {
                client_id
            })
            .then(_ => {
                fetchClients()
            })
            .catch(_ => {
            })
    }

    async function KickClientById(client_id) {
        if (!confirm('คุณแน่ใจ ?')) return false
        ServerService.manage
            .post('server/' + store.data?.id + '/clients/kick', {
                client_id
            })
            .then(_ => {
                fetchClients()
            })
            .catch(_ => {
            })
    }

    return (
        <React.Fragment>
            <div class="heading heading-border heading-middle-border heading-middle-border-center">
                <h3 class="font-weight-normal text-tertiary">
                    <strong class="font-weight-extra-bold">User Online</strong>
                </h3>
            </div>
            <table className="table table-bordered">
                <tbody>
                {clients.length === 0 && (
                    <tr>
                        <td className="text-center">ไม่มีผู้ใช้งานออนไลน์</td>
                    </tr>
                )}
                {clients.map(item => (
                    <tr>
                        <td>
                <span className="d-flex align-items-center">
                  {item?.nickname}
                </span>
                        </td>
                        <td>{item?.ip_address}</td>
                        <td>{item?.server_group}</td>
                        <td className="d-flex justify-content-center">
                            <button
                                onClick={e => KickClientById(item?.cid)}
                                className="btn btn-warning float-right btn-sm text-dark"
                            >
                                Kick
                            </button>
                            <button
                                onClick={e => BanClientById(item?.cid)}
                                className="btn btn-danger float-right btn-sm"
                            >
                                Ban
                            </button>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
            <div class="heading heading-border heading-middle-border heading-middle-border-center mt-4">
                <h3 class="font-weight-normal text-tertiary">
                    <strong class="font-weight-extra-bold">Ban list</strong>
                </h3>
            </div>
            <table className="table table-bordered">
                <thead>
                <td>ชื่อ / ไอพี</td>
                <td>รูปแบบ</td>
                <td>แบนเมื่อ</td>
                <td>ปลดเมื่อ</td>
                <td>ปลดแบน</td>
                </thead>
                <tbody>
                {bans.length === 0 && (
                    <tr>
                        <td colSpan={5}
                            className="text-center">
                            ไม่มีผู้ใช้งานถูกแบน
                        </td>
                    </tr>
                )}
                {bans.map(item => (
                    <tr>
                        <td>
                            <span className="d-flex align-items-center">{item.name}</span>
                        </td>
                        <td>{item?.type_message}</td>
                        <td>{item?.ban_at}</td>
                        <td>{item.duration}</td>
                        <td style={{ width: '100px' }}>
                            <button
                                onClick={_ => {
                                    return unbanClient(item.id)
                                }}
                                className="btn btn-tertiary float-right btn-sm"
                            >
                                ปลดแบน
                            </button>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
        </React.Fragment>
    )
}

export default ClientTab
