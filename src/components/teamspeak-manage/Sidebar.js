import React from 'react'
import { observer } from 'mobx-react'
import classnames from 'classnames'
import { Link } from 'react-router-dom'

const Sidebar = ({ tab, id }) => {
    return (
        <div className="col-lg-4">
            <div className="tabs tabs-vertical tabs-left tabs-navigation">
                <ul className="nav nav-tabs col-sm-3">
                    <li
                        class={classnames({
                            'nav-item': true,
                            active: tab === 'home'
                        })}
                    >
                        <Link
                            to={'/teamspeak/manage/' + id + '/home'}
                            className="nav-link text-dark"
                        >
                            <i className="fas fa-home"/> ข้อมูลรายละเอียดพื้นฐาน
                        </Link>
                    </li>

                    <li
                        class={classnames({
                            'nav-item': true,
                            active: tab === 'renew'
                        })}
                    >
                        <Link
                            to={'/teamspeak/manage/' + id + '/renew'}
                            className="nav-link text-dark"
                        >
                            <i className="fas fa-users"/> ต่ออายุการ & แก้ไขจำนวนผู้ใช้
                        </Link>
                    </li>
                    <li
                        class={classnames({
                            'nav-item': true,
                            active: tab === 'change_address'
                        })}
                    >
                        <Link
                            to={'/teamspeak/manage/' + id + '/change_address'}
                            className="nav-link text-dark"
                        >
                            <i className="fas fa-exchange-alt"/> เปลียนไอพี & เปลียนชื่อ
                        </Link>
                    </li>
                    <li
                        class={classnames({
                            'nav-item': true,
                            active: tab === 'privilege'
                        })}
                    >
                        <Link
                            to={'/teamspeak/manage/' + id + '/privilege'}
                            className="nav-link text-dark"
                        >
                            <i className="fas fa-lock"/> จัดการยศของเซิฟเวอร์
                        </Link>
                    </li>
                    <li
                        className={classnames({
                            'nav-item': true,
                            active: tab === 'clients'
                        })}
                    >
                        <Link
                            to={'/teamspeak/manage/' + id + '/clients'}
                            className="nav-link text-dark"
                        >
                            <i className="fas fa-users"/>
                            User (Poke/Kick/Ban)
                        </Link>
                    </li>
                    <li
                        class={classnames({
                            'nav-item': true,
                            active: tab === 'cancel'
                        })}
                    >
                        <Link
                            to={'/teamspeak/manage/' + id + '/cancel'}
                            className="nav-link text-danger"
                        >
                            <i className="fas fa-times"/> ยกเลิก
                        </Link>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default observer(Sidebar)
