import React, { useEffect, useState, Fragment } from 'react'
import ServerService from '../../services/ServerService'
import store from './store'
import { withRouter, Link } from 'react-router-dom'
const CancelTab = ({ history }) => {
  const [pincode, setPincode] = useState('')
  const submitDeleteThisServer = e => {
    e.preventDefault()
    if (confirm('การกระทำดังกล่าวไม่สามารถย้อนกลับได้ ')) {
      ServerService.manage
        .delete('server/' + store.data.id, {
          params: {
            pincode
          }
        })
        .then(_ => {
          history.push('/teamspeak/list')
        })
        .catch(_ => {})
    }
  }
  return (
    <Fragment>
      <div class="heading heading-border heading-middle-border heading-middle-border-center">
        <h3 class="font-weight-normal text-tertiary">
          <strong class="font-weight-extra-bold">CANCEL SERVICE</strong>
        </h3>
      </div>
      <div className="d-flex align-items-center flex-column  justify-content-center">
        <div className="form-group">
          <input
            type="number"
            placeholder="รหัสลับ 4 หลักของคุณ"
            value={pincode}
            className="form-control text-center"
            maxLength={4}
            onChange={({ target: { value } }) => {
              setPincode(value)
            }}
          />
        </div>
        <button
          onClick={submitDeleteThisServer}
          className="btn btn-danger btn-3d btn-lg"
          disabled={pincode.length !== 4}
        >
          <i className="fas fa-times fa-lg" />
          <br />
          ยกเลิกบริการ (ลบเซิฟเวอร์)
        </button>
      </div>
    </Fragment>
  )
}

export default withRouter(CancelTab)
