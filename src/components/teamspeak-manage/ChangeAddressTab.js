import React, { useEffect, useState } from 'react'
import { Observer } from 'mobx-react'
import store from './store'
import PackageService from '../../services/PackageService'
import ServerService from '../../services/ServerService'
import { NotificationManager } from 'react-notifications'
const ChangeAddressTab = () => {
  const [loading, setloading] = useState(false)
  const [domains, setdomains] = useState(['กำลังโหลด....'])
  const [domainName, setDomainName] = useState(store.data.dns)
  const [subDomain, setSubDomain] = useState(store.data.dnss)
  useEffect(() => {
    PackageService.findAllDomains()
      .then(({ data }) => {
        setdomains(data)
      })
      .catch(_ => {
        setdomains([])
      })
    return () => {}
  }, [])
  const submit = event => {
    event.preventDefault()
    const { id } = store.data
    setloading(true)
    ServerService.manage
      .put('server/' + id + '/ip_address', {
        ip_address: domainName,
        domain: subDomain
      })
      .then(({ data }) => {
        NotificationManager.success(data.message, 'สำเร็จ')
        store.fetchDetail()
        setloading(false)
      })
      .catch(_ => {
        setloading(false)
      })
  }
  return (
    <Observer>
      {() => (
        <React.Fragment>
          <div class=" heading heading-border heading-middle-border heading-middle-border-center">
            <h3 class="font-weight-normal text-tertiary">
              <strong class="font-weight-extra-bold">เปลียนแปลงไอพี</strong>
            </h3>
          </div>
          <div className="d-flex justify-content-center">
            <form onSubmit={submit} className="flex-row">
              <div className="form-group ">
                <div className="input-group d-flex justify-content-between">
                  <input
                    type="text"
                    className="form-control d-flex"
                    maxLength={20}
                    minLength={3}
                    placeholder="ตั้งชื่อไอพี"
                    value={domainName}
                    onChange={({ target: { value } }) => {
                      setDomainName(value)
                    }}
                    disabled={loading}
                  />
                  <div className="input-group-append" style={{ width: '40%' }}>
                    <select
                      onChange={({ target: { value } }) => {
                        setSubDomain(value)
                      }}
                      disabled={loading}
                      className="form-control"
                    >
                      {domains.map((item, key) => (
                        <option
                          value={item}
                          key={key}
                          selected={item === subDomain}
                        >
                          {item}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <button
                  type="submit"
                  className="btn btn-tertiary btn-block btn-lg "
                  disabled={loading}
                >
                  <i className="fas fa-check" /> เปลียนแปลง
                </button>
              </div>
            </form>
          </div>
        </React.Fragment>
      )}
    </Observer>
  )
}

export default ChangeAddressTab
