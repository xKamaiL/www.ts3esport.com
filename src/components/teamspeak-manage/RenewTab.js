import React, { useEffect, useState } from 'react'
import { observer, Observer } from 'mobx-react'
import store from './store'
import { NotificationManager } from 'react-notifications'
import ServerService from '../../services/ServerService'
import userStore from '../userStore'
import PackageService from '../../services/PackageService'

const Message = ({ children }) => (
  <blockquote className="blockquote-primary">
    <p className="mb-0">
      <i class="fa fa-lg fa-star fa-spin" /> {children}
    </p>
  </blockquote>
)

const RenewTab = () => {
  const [userCount, setUserCount] = useState(null)
  const [plans, setPlans] = useState([])
  const [currentPlanId, setCurrentPlanId] = useState(null)
  useEffect(() => {
    PackageService.findAllPackage()
      .then(({ data }) => {
        setPlans(data)
        setCurrentPlanId(
          data.filter(a => a.plan_id == store.data.plan_id)[0].plan_id
        )
      })
      .catch(_ => {
        setPlans([])
      })
    setUserCount(store.data.maxclients)
  }, [])
  const submitRenew = (event, mode) => {
    const { id } = store
    event.preventDefault()
    if (mode === 1 && userCount <= store.data.maxclients) {
      return NotificationManager.warning(
        'จำนวนผู้ใช้ต้องไม่ต่ำกว่าเดิม',
        'คำเตือน'
      )
    }
    if (mode === 2 && userCount < 30) {
      return NotificationManager.warning(
        'จำนวนผู้ใช้ต้องไม่ต่ำกว่า 30',
        'คำเตือน'
      )
    }
    if (
      confirm('คุณแน่ใจที่จะต่ออายุ ? การกระทำดังกล่าวไม่สามารถย้อนกลับได้')
    ) {
      return ServerService.manage
        .put('server/' + id + '/renew', {
          mode: mode,
          user_count: userCount,
          plan_id: currentPlanId
        })
        .then(({ data }) => {
          NotificationManager.success('', data.message)
          store.fetchDetail()
          userStore.getProfile()
        })
        .catch(_ => {})
    }
  }
  return (
    <Observer>
      {_ => (
        <>
          <div class="heading heading-border heading-middle-border heading-middle-border-center">
            <h3 class="font-weight-normal text-tertiary">
              <strong class="font-weight-extra-bold">
                จะหมดอายุใน {store.data.expire_message_th}
              </strong>
            </h3>
          </div>
          <div className="row d-flex justify-content-center">
            <div className="col-md-6 col-12 mb-4">
              <Message>
                ต่ออายุการใช้งานทันที่ 30 วัน, ไม่เพิ่ม/ลด จำนวนผู้ใช้งาน.
              </Message>
              <div className="form-group">
                <select
                  className="form-control"
                  onChange={({ target: { value } }) => {
                    setCurrentPlanId(value)
                  }}
                >
                  {plans.map(({ plan_id, plan_name, price }) => (
                    <option
                      value={plan_id}
                      selected={plan_id === currentPlanId}
                    >
                      {plan_name} ({price} บาท)
                    </option>
                  ))}
                </select>
              </div>
              <h3 class="text-center mb-3">
                <i class="fa fa-lg fa-money" /> ราคา{' '}
                <b>
                  {currentPlanId !== null
                    ? plans.filter(a => a.plan_id == currentPlanId)[0].price
                    : 0}
                </b>{' '}
                บาท (
                {currentPlanId !== null
                  ? plans.filter(a => a.plan_id == currentPlanId)[0].day_use
                  : 0}{' '}
                วัน)
              </h3>
              <button
                onClick={e => submitRenew(e, 0)}
                className="btn btn-tertiary btn-block"
              >
                ต่ออายุการใช้งาน
              </button>
            </div>
            <div className="col-12">
              <div class="divider">
                <i class="fas fa-bolt" />
              </div>
            </div>
            {/*
<div className="col-md-6 col-12 mb-4">
              <Message>
                ปรับจำนวนผู้ใช้, ไม่เพิ่มจำนวนวัน หรือ ต่ออายุการใช้งาน.
              </Message>
              <h3 class="text-center mb-3">
                <i class="fa fa-lg fa-money" /> ราคา{' '}
                <b>
                  {userCount > store.data.maxclients
                    ? (userCount - store.data.maxclients) * 1.5
                    : 0}
                </b>{' '}
                บาท
              </h3>
              <div className="form-group">
                <input
                  type="number"
                  placeholder="จำนวนผู้ใช้งาน"
                  value={userCount}
                  onChange={({ target: { value } }) => {
                    setUserCount(value)
                  }}
                  className="form-control text-center"
                />
              </div>
              <button
                onClick={e => submitRenew(e, 1)}
                className="btn btn-tertiary btn-block"
              >
                ยืนยัน แก้ไข
              </button>
            </div>
             <div className="col-12">
              <div class="divider">
                <i class="fas fa-bolt" />
              </div>
            </div>
            <div className="col-md-6 col-12 mb-4">
              <Message>ปรับจำนวนผู้ใช้ พร้อม ต่ออายุการใช้งาน 30 วัน.</Message>
              <h3 class="text-center mb-3">
                <i class="fa fa-lg fa-money" /> ราคา <b>{userCount * 1.5}</b>{' '}
                บาท
              </h3>
              <div className="form-group">
                <input
                  type="number"
                  placeholder="จำนวนผู้ใช้งาน"
                  value={userCount}
                  onChange={({ target: { value } }) => {
                    setUserCount(value)
                  }}
                  className="form-control text-center"
                />
              </div>
              <button
                onClick={e => submitRenew(e, 2)}
                className="btn btn-tertiary btn-block"
              >
                ต่ออายุการใช้งานทันที
              </button>
            </div>
              */}
          </div>
        </>
      )}
    </Observer>
  )
}

export default RenewTab
