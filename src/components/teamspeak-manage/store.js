import { observable, action } from 'mobx'
import ServerService from '../../services/ServerService'

class teamspeakStore {
  @observable id = null
  @observable isError = false
  @observable data = null
  @observable loading = true
  @observable settings = null
  @action
  fetchDetail = async () => {
    await ServerService.findById(this.id)
      .then(({ data }) => {
        this.data = data
        document.title = 'จัดการ ' + data.domain
      })
      .catch(_ => {
        this.isError = true
      })
  }
}

const store = new teamspeakStore()
export default store
