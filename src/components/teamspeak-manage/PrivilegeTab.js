import React, { useEffect, useState } from 'react'
import ServerService from '../../services/ServerService'
import store from './store'
import { NotificationManager } from 'react-notifications'
import ClientModal from './ClientModal'
import { observer } from 'mobx-react'

const ClientItem = observer(
  ({ cid, nickname, server_group_id, server_group, toggle, token }) => {
    const { id } = store.data

    const addPermisssion = e => {
      e.preventDefault()
      ServerService.manage
        .post('server/' + id + '/privilege_key/client', {
          client_id: cid,
          server_group_id,
          token
        })
        .then(({ data }) => {
          NotificationManager.success('', data.message)
          toggle()
        })
        .catch(_ => {})
    }
    return (
      <tr>
        <td>{cid}</td>
        <td>{nickname}</td>
        <td>{server_group}</td>
        <td>
          <button
            onClick={addPermisssion}
            className="btn btn-sm btn-tertiary btn-block"
          >
            <i className="fas fa-plus" /> มอบยศให้
          </button>
        </td>
      </tr>
    )
  }
)
const PrivilegeTab = () => {
  const [privilege, setPrivilege] = useState([])
  const [groups, setGroups] = useState([])
  const [selectPrivilege, setSelectPrivilege] = useState(null)
  const { id } = store.data
  function fetchKey() {
    return ServerService.manage
      .get('server/' + id + '/privilege_key')
      .then(({ data }) => {
        setPrivilege(data)
      })
      .catch(_ => {
        setPrivilege([])
      })
  }
  useEffect(() => {
    fetchKey()
    ServerService.manage
      .get('server/' + id + '/privilege_key/group')
      .then(({ data }) => {
        setGroups(data)
        setSelectPrivilege(data[0].sgid)
      })
      .catch(_ => {
        setGroups([])
      })
    return () => {}
  }, [])

  const createAdminQueryPrivilegeKey = event => {
    const l = new Loading()
    ServerService.manage
      .post('server/' + id + '/privilege_key/admin_query')
      .then(() => {
        fetchKey()
        l.out()
      })
      .catch(_ => {
        fetchKey()
        l.out()
      })
  }
  const createPrivilegeKey = () => {
    const l = new Loading()
    ServerService.manage
      .post('server/' + id + '/privilege_key', {
        sgid: selectPrivilege
      })
      .then(({ data }) => {
        fetchKey()
        l.out()
      })
      .catch(_ => {
        fetchKey()
        l.out()
      })
  }
  const removePrivilege = token => {
    ServerService.manage
      .post('server/' + id + '/privilege_key/delete', {
        token
      })
      .then(({ data }) => {
        NotificationManager.success('', data.message)
        return fetchKey()
      })
      .catch(_ => {})
  }
  const resetPrivilegeKey = event => {
    event.preventDefault()
    if (confirm('คุณแน่ใจ ?')) {
      ServerService.manage
        .delete('server/' + id + '/privilege_key/reset')
        .then(({ data }) => {
          return fetchKey()
        })
        .catch(_ => {})
    }
  }
  const [modal, setModal] = useState(false)
  const toggle = async () => {
    await fetchKey()
    setModal(!modal)
    setCurrentToken({
      token: '',
      id: null
    })
  }
  const [currentToken, setCurrentToken] = useState(null)
  const addKeyToClient = (token, id) => {
    setModal(true)
    setCurrentToken({ token, id })
  }
  return (
    <>
      <div class="heading heading-border heading-middle-border heading-middle-border-center">
        <h3 class="font-weight-normal text-tertiary">
          <strong class="font-weight-extra-bold">ยศของเซิฟเวอร์</strong>
        </h3>
      </div>
      <div className="row">
        <div className="col-md-6">
          <h3 className="text-5 mb-2">New Privilege Key</h3>
          <div class="form-group">
            <select
              class="form-control text-center"
              value={selectPrivilege}
              onChange={({ target: { value } }) => {
                setSelectPrivilege(value)
              }}
              name="name"
            >
              {groups.map(item => (
                <option value={item.sgid}>{item.name}</option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <button
              onClick={createPrivilegeKey}
              className="btn btn-tertiary btn-block "
            >
              สร้างใหม่
            </button>
          </div>
        </div>
        <div className="col-md-6">
          <h3 className="text-5 mb-2">
            <span className="text-danger">*</span> New Admin Query
          </h3>
          <div class="form-group">
            <select class="form-control text-center" disabled>
              <option selected>Admin Query</option>
            </select>
          </div>
          <div className="form-group">
            <button
              onClick={createAdminQueryPrivilegeKey}
              className="btn btn-danger btn-block"
            >
              สร้างใหม่
            </button>
          </div>
        </div>
        <div className="col-12 mt-4">
          <h3 className="text-5 mb-2">
            <span className="text-danger">*</span> Privilege Key List
            <button
              className="btn btn-danger btn-square btn-sm float-right align-items-center mr-2 mt-0"
              onClick={resetPrivilegeKey}
            >
              <i className="fas fa-sync" /> รีเซ็ตยศ
            </button>
          </h3>
          <div className="table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th class="center">ชื่อยศ</th>
                  <th class="center">Privilege Key</th>
                  <th class="center">สร้างเมื่อ</th>
                  <th class="center" width="7%" />
                </tr>
              </thead>
              <tbody>
                {privilege.map(item => (
                  <tr key={item.token}>
                    <td class="center text-beat align-items-center">
                      {item.group_name}
                    </td>
                    <td class="center align-items-center">
                      <p>{item.token}</p>
                    </td>
                    <td class="center align-items-center">{item.created_at}</td>
                    <td class="center align-items-center">
                      <button
                        type="submit"
                        onClick={e => {
                          e.preventDefault()
                          return addKeyToClient(
                            item.token,
                            item.server_group_id
                          )
                        }}
                        class=" btn  btn-sm"
                      >
                        <i class="fas fa-plus text-success" />
                      </button>
                      <button
                        type="submit"
                        onClick={() => {
                          removePrivilege(item.token)
                        }}
                        class=" btn  btn-sm"
                      >
                        <i class="fas fa-trash text-danger" />
                      </button>
                    </td>
                  </tr>
                ))}
                {privilege.length === 0 && (
                  <tr>
                    <td colSpan={4} className="text-center">
                      ไม่มี
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {modal && (
        <ClientModal
          title={'แอดยศให้ยูสเซอร์ (' + currentToken.token + ')'}
          toggle={toggle}
        >
          {client => (
            <table class="table table-striped">
              <tbody>
                {client.map(item => (
                  <ClientItem
                    {...item}
                    server_group_id={currentToken.id}
                    toggle={toggle}
                    token={currentToken.token}
                  />
                ))}
              </tbody>
            </table>
          )}
        </ClientModal>
      )}
    </>
  )
}

export default PrivilegeTab
