import React, { useEffect, useState } from 'react'
import store from './store'
import moment from 'moment'
import ServerService from '../../services/ServerService'
import { NotificationManager } from 'react-notifications'

const DashbaordTab = ({ setLoading }) => {
  useEffect(() => {
    store.loading = false
  }, [])
  const {
    domain,
    expire,
    expire_message_th,
    ip_address_number,
    first_token,
    teamspeak_server_name
  } = store.data
  return (
    <React.Fragment>
      <div class="heading heading-border heading-middle-border heading-middle-border-center">
        <h3 class="font-weight-normal text-tertiary">
          <strong class="font-weight-extra-bold">ข้อมูลเบื้องต้น</strong>
        </h3>
      </div>

      <table class="table m-0">
        <tbody>
          <tr>
            <th class="border-top-0">ที่อยู่ไอพี:</th>
            <td class="border-top-0 text-right">
              <a
                href={'ts3server://' + domain}
                className="btn btn-outline-dark"
              >
                {domain}
              </a>
            </td>
          </tr>
          <tr>
            <th>หมดอายุเมื่อ</th>
            <td className="text-right">
              {moment(expire).format('DD-MM-YYYY hh:mm')}{' '}
              <small>({expire_message_th})</small>
            </td>
          </tr>
          <tr>
            <th>ไอพีตัวเลข</th>
            <td className="text-right">{ip_address_number}</td>
          </tr>
          <tr>
            <th>เครื่อง</th>
            <td className="text-right">{teamspeak_server_name}</td>
          </tr>
          <tr>
            <th>ยศเริ่มต้น</th>
            <td className="text-right">
              {first_token === null ? (
                <span>
                  <i className="fas fa-times text-danger" /> ถูกใช้งานไปแล้ว
                </span>
              ) : (
                <a
                  href={
                    'ts3server://' + ip_address_number + '?token=' + first_token
                  }
                  className="btn btn-success btn-3d"
                  onClick={_ => {
                    store.fetchDetail()
                  }}
                >
                  ใช้งานยศครั้งแรก
                </a>
              )}
            </td>
          </tr>
        </tbody>
      </table>
      <div class="heading heading-border heading-middle-border heading-middle-border-center">
        <h3 class="font-weight-normal text-tertiary">
          <strong class="font-weight-extra-bold">ข้อมูลเบื้องต้น</strong>
        </h3>
      </div>
      <div className="form-group text-center">
        <label htmlFor="uploadBanner" className="text-dark text-center">
          อัพโหลดแบนเนอร์
        </label>
        <input
          type="file"
          className="form-control custom-line-height-1"
          style={{ lineHeight: 1 }}
          onChange={e => {
            const {
              target: { files }
            } = e
            const file = files[files.length - 1]
            const data = new FormData()
            data.append('image', file)
            ServerService.manage
              .post('server/' + store.data.id + '/banner', data)
              .then(({ data }) => {
                NotificationManager.success('', data.message)
                e.target.value = ''
              })
              .catch(_ => {})
          }}
        />
      </div>
    </React.Fragment>
  )
}

export default DashbaordTab
