import React, { useEffect, useState } from 'react'
import classnames from 'classnames'
import LoginForm from '../ui/LoginForm'
import userStore from '../userStore'
import { Observer } from 'mobx-react'
const LoginModal = () => {
  const setopen = value => {
    userStore.login_modal = value
  }
  const [wrapperRef, setwrapperRef] = useState(null)
  const toggle = e => {
    setopen(!userStore.login_modal)
  }

  useEffect(() => {
    const handleClickOutside = event => {
      if (wrapperRef && !wrapperRef.contains(event.target)) {
        setopen(false)
      }
    }
    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [wrapperRef])
  return (
    <Observer>
      {_ => (
        <div className="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">
          <div
            className="header-nav-feature header-nav-features-user d-inline-flex mx-2 pr-2 signin"
            id="headerAccount"
          >
            <a href="#" className="header-nav-features-toggle" onClick={toggle}>
              <i className="far fa-user" /> ล็อคอิน
            </a>
            <div
              className={classnames(
                'header-nav-features-dropdown header-nav-features-dropdown-mobile-fixed header-nav-features-dropdown-force-right',
                { show: userStore.login_modal }
              )}
              id="headerTopUserDropdown"
              ref={e => {
                setwrapperRef(e)
              }}
            >
              <div className="signin-form" style={{ display: 'block' }}>
                <LoginForm />
              </div>
            </div>
          </div>
        </div>
      )}
    </Observer>
  )
}

export default LoginModal
