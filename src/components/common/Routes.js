import React, { useEffect } from 'react'
import { Route, Switch } from 'react-router-dom'
import Home from '../home'
import PageNotFound from '../ui/PageNotFound'
import userStore from '../userStore'
import { withRouter, Link } from 'react-router-dom'
import { observer } from 'mobx-react'
import RegisterPage from '../register'
import TeamspeakCreate from '../teamspeak-create'
import NoAuthenticate from '../ui/NoAuthenticate'
import TeamspeakServerList from '../teamspeak-serverlist'
import TeamspeakManage from '../teamspeak-manage'
import TopupPage from '../topup'
import PageInternalError from '../ui/PageInternalError'
import DownloadTable from '../ui/DownloadTable'
import ProfilePage from '../profile'
import NewShopItem from '../profile/NewShopItem'
import NewPromoteServer from '../profile/NewPromoteServer'
import PromoteServerIndexPage from '../promote-index'
import PromoteServerIndexGames from '../promote-index/games'
import PromoteServerDetails from '../promote-server'
import ShopAllPage from '../shop-all'
import ShopDetails from '../shop-details'
import ForgotPassword from '../forgot-password'
import ResetPassword from '../reset-password'

const withAuthen = BodyComponent =>
  withRouter(
    observer(
      class WrapperComponent extends React.Component {
        state = { loading: true }
        async componentDidMount() {
          await userStore.getProfile().then(() => {
            if (userStore.isLogin === false) {
              userStore.login_modal = true
            }
          })
          this.setState({ loading: false })
        }
        render() {
          if (this.state.loading && userStore.firstLoadApp)
            return <React.Fragment />
          if (!userStore.isLogin) {
            return <NoAuthenticate />
          } else {
            return <BodyComponent {...this.props} />
          }
        }
      }
    )
  )

function Routes({ history }) {
  useEffect(() => {
    history.listen(() => {
      window.scrollTo(0, 0)
    })
    return () => {}
  }, [history])
  return (
    <React.Fragment>
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/register' exact component={RegisterPage} />
        <Route path='/not-found' component={PageNotFound} />
        <Route
          path='/teamspeak/create'
          exact
          component={withAuthen(TeamspeakCreate)}
        />
        <Route
          path='/teamspeak/list'
          exact
          component={withAuthen(TeamspeakServerList)}
        />
        <Route
          path='/teamspeak/manage/:id/:tab'
          exact
          component={withAuthen(TeamspeakManage)}
        />
        <Route path='/topup' exact component={withAuthen(TopupPage)} />
        <Route path='/profile' exact component={withAuthen(ProfilePage)} />
        <Route
          path='/profile/new-promote-server'
          exact
          component={withAuthen(NewPromoteServer)}
        />
        <Route
          path='/profile/new-shop-item'
          exact
          component={withAuthen(NewShopItem)}
        />
        <Route
          path='/profile/promote-server/:id'
          exact
          component={withAuthen(NewPromoteServer)}
        />
        <Route
          path='/profile/shop-item/:id'
          exact
          component={withAuthen(NewShopItem)}
        />
        <Route path='/promote' exact component={PromoteServerIndexPage} />
        <Route
          path='/promote/:game'
          exact
          component={PromoteServerIndexGames}
        />
        <Route path='/forgot-password' exact component={ForgotPassword} />
        <Route path='/reset-password' exact component={ResetPassword} />
        <Route
          path='/promote/server/:server_id'
          exact
          component={PromoteServerDetails}
        />
        <Route path='/shop/all' exact component={ShopAllPage} />
        <Route path='/shop/:id' exact component={ShopDetails} />
        <Route path='/unexpected-error' component={PageInternalError} exact />
        <Route path='/download' component={DownloadTable} exact />
        <Route
          render={() => {
            history.push('/not-found')
          }}
        />
      </Switch>
    </React.Fragment>
  )
}
export default withRouter(Routes)
