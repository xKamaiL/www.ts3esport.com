import React from 'react'
import ReactDOM from 'react-dom'
import { createBrowserHistory } from 'history'
import { Provider, observer } from 'mobx-react'
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'
import { BrowserRouter } from 'react-router-dom'
import Routes from './Routes'
import { NotificationContainer } from 'react-notifications'
import userStore from '../userStore'
import SidebarApp from './Sidebar'
import { hot } from 'react-hot-loader/root'
import { setConfig } from 'react-hot-loader'
import Header from './Header'
import Footer from '../ui/Footer'
import { withRouter, Link } from 'react-router-dom'
const browserHistory = createBrowserHistory()
setConfig({
  hotHooks: true
})
export const routingStore = new RouterStore()
const stores = {
  routing: routingStore
}
const history = syncHistoryWithStore(browserHistory, routingStore)
class App extends React.Component {
  state = {
    loading: true
  }
  async componentDidMount() {
    await userStore.getProfile()
    this.setState({ loading: false })
  }

  render() {
    return (
      <Provider {...stores}>
        <BrowserRouter history={history}>
          <Header loading={this.state.loading} />
          <div className="main" role="main">
            <Routes />
          </div>
          <NotificationContainer />
          <Footer />
        </BrowserRouter>
      </Provider>
    )
  }
}

export default hot(observer(App))
