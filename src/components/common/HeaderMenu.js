import React from 'react'
import Skeleton from 'react-skeleton-loader'
import { Link } from 'react-router-dom'
import { Observer } from 'mobx-react'
import userStore from '../userStore'
import UserMenu from '../ui/UserMenu'

const LoadingMenu = () => (
    <React.Fragment>
        <li className="">
            <a className="dropdown-item">
                <Skeleton/>
            </a>
        </li>
        <li className="">
            <a className="dropdown-item">
                <Skeleton/>
            </a>
        </li>
        <li className="">
            <a className="dropdown-item">
                <Skeleton/>
            </a>
        </li>
        <li className="">
            <a className="dropdown-item">
                <Skeleton/>
            </a>
        </li>
    </React.Fragment>
)

const HeaderMenu = ({ loading }) => {
    if (loading) return <LoadingMenu/>
    return (
        <Observer>
            {(props) => (
                <React.Fragment>
                    <li>
                        <Link to="/"
                              className="dropdown-item">
                            <i className="fas fa-home mr-2"/> หน้าแรก
                        </Link>
                    </li>
                    {/*<li>*/}
                    {/*    <Link to="/shop/all"*/}
                    {/*          className="text-success">*/}
                    {/*        <i className="fas fa-shopping-cart mr-2"/>*/}
                    {/*        ซื้อขายไอดีเกม*/}
                    {/*    </Link>*/}
                    {/*</li>*/}
                    {/*<li>*/}
                    {/*    <Link to="/promote"*/}
                    {/*          className="text-success">*/}
                    {/*        <i className="fas fa-bullhorn mr-2"/>*/}
                    {/*        โปรโมทเซิร์ฟเวอร์เกม / อื่น ๆ*/}
                    {/*    </Link>*/}
                    {/*</li>*/}
                    <li className="dropdown">
                        <a
                            className="dropdown-item"
                            href=""
                            onClick={(e) => e.preventDefault()}
                        >
                            <i className="fas fa-server mr-2"/>
                            ทีเอสสาม
                        </a>
                        <ul className="dropdown-menu">
                            <li>
                                <Link to="/teamspeak/list"
                                      className="dropdown-item">
                                    <i className="fas fa-list mr-2"/> <b>เซิฟเวอร์ของฉัน</b>
                                </Link>
                            </li>
                            <li>
                                <Link to="/teamspeak/create"
                                      className="dropdown-item">
                                    <i className="fas fa-plus mr-2"/> เช่าเซิฟเวอร์
                                </Link>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <Link className="dropdown-item"
                              to="/topup">
                            <i className="fas fa-coins mr-2"/>
                            เติมเงิน
                        </Link>
                    </li>
                    <li>
                        <Link className="dropdown-item"
                              to="/download">
                            <i className="fas fa-download mr-2"/>
                            ดาวโหลด
                        </Link>
                    </li>
                    {userStore.isLogin && <UserMenu/>}
                </React.Fragment>
            )}
        </Observer>
    )
}

export default HeaderMenu
