import React, { useEffect, useState } from 'react'
import Skeleton from 'react-skeleton-loader'
import HeaderMenu from './HeaderMenu'

import { withRouter, Link } from 'react-router-dom'
import userStore from '../userStore'
import LoginModal from './LoginModal'
import { observer } from 'mobx-react'

const Header = props => {
  return (
    <header
      id="header"
      className="header-effect-shrink"
      data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}"
    >
      <div className="header-body">
        <div className="header-container container-fluid">
          <div className="header-row">
            <div className="header-column">
              <div className="header-row">
                <div className="header-logo">
                  <Link to="/">
                    <img
                      width={100}
                      data-sticky-width={82}
                      data-sticky-height={40}
                      src="/assets/img/logo.png"
                    />
                  </Link>
                </div>
              </div>
            </div>
            <div className="header-column justify-content-end">
              <div className="header-row">
                <div className="header-nav header-nav-line header-nav-top-line header-nav-top-line-with-border order-2 order-lg-1">
                  <div className="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                    <nav className="collapse">
                      <ul className="nav nav-pills" id="mainNav">
                        <HeaderMenu {...props} />
                      </ul>
                    </nav>
                  </div>
                  <button
                    className="btn header-btn-collapse-nav"
                    data-toggle="collapse"
                    data-target=".header-nav-main nav"
                  >
                    <i className="fas fa-bars" />
                  </button>
                </div>
              </div>
            </div>
            {!props.loading && !userStore.isLogin && <LoginModal />}
          </div>
        </div>
      </div>
    </header>
  )
}

export default observer(Header)
