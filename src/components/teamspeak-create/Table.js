import React, { useEffect, useState } from 'react'
import { observer } from 'mobx-react'
import store from './store'
import userStore from '../userStore'

const TableSelling = () => {
  const findPackageById = id => {
    const data = store.packages.findIndex(item => {
      return item.plan_id === id
    })
    return store.packages[data]
  }
  const findServerById = id => {
    const data = store.available_server.findIndex(item => {
      return item.id === id
    })
    return store.available_server[data]
  }
  const {
    ip_address,
    domain,
    is_selected,
    plan_id,
    users_count,
    server_id
  } = store.buy_data
  //  if (is_selected === null || (plan_id === null && is_selected === true))
  if (plan_id === null)
    return (
      <div className='d-flex justify-content-center p-5 my-5'>
        <h4>
          <i className='fas fa-times' /> กรุณาเลือกแพคเกจ
        </h4>
      </div>
    )
  return (
    <table class='table table-striped text-center'>
      <thead>
        <tr>
          <th style={{ width: '50%' }} className=' text-left'>
            หัวข้อ
          </th>
          <th style={{ width: '50%' }} className=' text-cebter'>
            รายระเอียด
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td className=' text-left'>แพคเกจ</td>
          <td>
            {is_selected === false
              ? 'กำหนดเอง'
              : findPackageById(plan_id).plan_name}
          </td>
        </tr>
        <tr>
          <td className=' text-left'>จำนวนผู้ใช้งาน</td>
          <td>
            <b>
              {!is_selected ? users_count : findPackageById(plan_id).maxclients}{' '}
              คน
            </b>
          </td>
        </tr>
        <tr>
          <td className=' text-left'>จำนวนวันที่ได้รับ</td>
          <td>{!is_selected ? '30' : findPackageById(plan_id).day_use} วัน</td>
        </tr>
        <tr>
          <td className=' text-left'>เครื่องเซิฟเวอร์</td>
          <td>{server_id && findServerById(server_id).name}</td>
        </tr>
        <tr>
          <td className=' text-left'>ไอพี</td>
          <td>
            {ip_address}.{domain}{' '}
            {store.domain_isvalid ? (
              <i className='fas fa-check text-success' />
            ) : (
              <i className='fas fa-times text-danger' />
            )}
          </td>
        </tr>
        <tr>
          <td className=' text-left'>ราคา</td>
          <td>
            <b className='text-3'>
              {userStore.userData.has_free == 1
                ? 0
                : is_selected === false
                ? parseInt(users_count) * 1.5
                : findPackageById(plan_id).price}{' '}
              บาท
            </b>
          </td>
        </tr>
      </tbody>
    </table>
  )
}

export default observer(TableSelling)
