import { action, observable } from 'mobx'
import PackageService from '../../services/PackageService'
import { NotificationManager } from 'react-notifications'

class createServerStore {
    @observable buy_data = {
        users_count: null,
        domain: '',
        ip_address: '',
        server_id: '',
        is_selected: true,
        plan_id: null
    }
    @observable available_server = []
    @observable packages = []
    @observable domains = []
    @action
    fetchPackages = async () => {
        return PackageService.findAllPackage()
            .then(({ data }) => {
                this.packages = data
            })
            .catch(_ => {
                this.packages = []
            })
    }
    @observable domain_isvalid = false
    @action
    checkDomainIsValid = () => {
        return PackageService.checkDomain(this.buy_data)
            .then(({ data }) => {
                this.domain_isvalid = true
            })
            .catch(_ => {
                this.domain_isvalid = false
            })
    }
    @action
    fetchDomains = () => {
        return PackageService.findAllDomains()
            .then(({ data }) => {
                this.domains = data
                if (data.length > 0) {
                    this.buy_data.domain = data[0]
                }
            })
            .catch(_ => {
                this.domains = []
            })
    }
    @action
    fetchServers = () => {
        return PackageService.findAllServers()
            .then(({ data }) => {
                this.available_server = data
                //if (data.length === 1) {
                this.buy_data.server_id = data[0].id
            })
            .catch(_ => {
                this.available_server = []
            })
    }
    @action
    submitBuyTeamspeak = async event => {
        event.preventDefault()
        const loading = new Loading()
        if (!this.buy_data.is_selected) {
            delete this.buy_data?.is_selected
        } else {
            this.buy_data.is_selected = '1'
        }
        if (!this.buy_data?.users_count) {
            this.buy_data.users_count = '30'
        }

        await PackageService.buy(this.buy_data)
            .then(({ data }) => {
                NotificationManager.success('สร้างเซิฟเวอร์ใหม่สำเร็จ', 'ยินดีด้วย')
                window.location.href = '/teamspeak/list'
            })
            .catch(_ => {
            })
        loading.out()
    }
}

const store = new createServerStore()
export default store
