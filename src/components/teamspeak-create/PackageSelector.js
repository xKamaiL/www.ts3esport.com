import React, { useEffect, useState } from 'react'
import PackageCustom from './PackageCustom'
import store from './store'
import { observer } from 'mobx-react'
import PackageItem from './PackageItem'

const SelectButton = ({ children, onClick, color }) => (
  <a
    onClick={onClick}
    href="#"
    class={`call-to-action call-to-action-${color} mb-5 text-center`}
  >
    <div class="call-to-action-content mx-auto">{children}</div>
  </a>
)

const PackageSelector = () => {
  const { is_selected } = store.buy_data
  const { packages } = store
  return (
    <div className="row">
      {packages.map(item => (
        <PackageItem {...item} key={item.id} />
      ))}
      {/* 
     <div className="col-12 d-flex justify-content-end ">
        <button className="ml-auto btn btn-danger" onClick={clearState}>
          <i className="fas fa-backward" /> ย้อนกลับ
        </button>
      </div>
    */}
    </div>
  )
  const clearState = event => {
    event.preventDefault()
    store.buy_data.is_selected = null
    store.buy_data.plan_id = null
  }
  if (is_selected === null) {
    return (
      <div className="row">
        <div className="col">
          <SelectButton
            color={'primary'}
            onClick={e => {
              e.preventDefault()
              store.buy_data.is_selected = true
            }}
          >
            <h3>แสดงรายการแพคเกจทั้งหมด</h3>
            <p class="mb-0 opacity-7">เลือกแพคเกจที่มีอยู่ในระบบ</p>
          </SelectButton>
        </div>
        <div className="col">
          <SelectButton
            color={'tertiary'}
            onClick={e => {
              e.preventDefault()
              store.buy_data.is_selected = false
            }}
          >
            <h3>กำหนดจำนวนผู้ใช้งานเอง</h3>
            <p class="mb-0 opacity-7">คำนวนค่าใช้จ่ายให้เอง</p>
          </SelectButton>
        </div>
      </div>
    )
  }
  if (is_selected === false) {
    return (
      <React.Fragment>
        <PackageCustom />
        <div className="col-12 d-flex justify-content-end ">
          <button className="ml-auto btn btn-danger" onClick={clearState}>
            <i className="fas fa-backward" /> ย้อนกลับ
          </button>
        </div>
      </React.Fragment>
    )
  } else {
    return (
      <div className="row">
        {packages.map(item => (
          <PackageItem {...item} key={item.id} />
        ))}
        {/* 
       <div className="col-12 d-flex justify-content-end ">
          <button className="ml-auto btn btn-danger" onClick={clearState}>
            <i className="fas fa-backward" /> ย้อนกลับ
          </button>
        </div>
      */}
      </div>
    )
  }
}

export default observer(PackageSelector)
