import React, { useEffect, useState } from 'react'
import store from './store'
import { observer } from 'mobx-react'

const PackageItem = ({ plan_name, plan_id, price, day_use, maxclients }) => {
  const active = store.buy_data.plan_id === plan_id
  return (
    <div class="col-lg-3 col-sm-6">
      <div class="featured-box featured-box-primary featured-box-effect-3">
        <div class="box-content box-content-border-0 px-3 py-2">
          <h4 class="font-weight-normal text-5 text-danger ">{plan_name}</h4>
          <p class="mb-2 mt-2 text-3 text-dark">
            ราคา <b>{price}</b> บาท / <b>{maxclients}</b> คน
            <br /> <b>{day_use}</b> วัน
          </p>
          {active ? (
            <button className="btn btn-success btn-block" disabled>
              เลือกเลย
            </button>
          ) : (
            <button
              onClick={e => {
                e.preventDefault()
                store.buy_data.plan_id = plan_id
              }}
              className="btn btn-outline-success btn-block"
            >
              เลือกเลย
            </button>
          )}
        </div>
      </div>
    </div>
  )
}

export default observer(PackageItem)
