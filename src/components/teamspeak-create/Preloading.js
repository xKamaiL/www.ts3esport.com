import React, { useEffect, useState } from 'react'
import { PageHeader } from '.'
import Skeleton from 'react-skeleton-loader'

const Preloading = () => {
  return (
    <React.Fragment>
      <PageHeader>New Teamspeak 3 Server</PageHeader>
      <div className="container">
        <div class="heading-middle-border">
          <h2 class="font-weight-normal">
            <Skeleton />
          </h2>
        </div>
        <div className="row">
          {[1, 2].map(() => (
            <div className="col mb-5">
              <Skeleton height="124px" width="100%" widthRandomness="0" />
            </div>
          ))}
        </div>
        <div class="heading-middle-border">
          <h2 class="font-weight-normal">
            <Skeleton />
          </h2>
        </div>
        <div className="row d-flex justify-content-center">
          {[...Array(2)].map(() => (
            <div className="col-md-4">
              <Skeleton height="210px" width="100%" widthRandomness="0" />
            </div>
          ))}
        </div>
        <div class="heading-middle-border">
          <h2 class="font-weight-normal">
            <Skeleton />
          </h2>
        </div>
        <div className="row mb-5">
          <div className="col-md-8">
            <Skeleton height="40px" width="100%" />
          </div>
          <div className="col-md-4">
            <Skeleton height="40px" width="100%" />
          </div>
        </div>
        <div class="heading-middle-border">
          <h2 class="font-weight-normal">
            <Skeleton />
          </h2>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Preloading
