import React, { useEffect, useState } from 'react'
import { Observer, observer } from 'mobx-react'
import store from './store'

const PackageCustom = ({}) => {
  const { users_count } = store.buy_data
  return (
    <Observer>
      {() => (
        <div class="d-flex">
          <div class="featured-box featured-box-primary featured-box-effect-3">
            <div class="box-content box-content-border-0 px-3 py-2">
              <h4 class="font-weight-normal text-5 ">กำหนดเอง </h4>
              <p class="mb-0 mt-2 text-3 text-dark">
                ราคา{' '}
                <b>
                  {Number.isInteger(parseInt(users_count))
                    ? parseInt(users_count) * 1.5
                    : 0}
                </b>{' '}
                บาท
              </p>
              <p className="mb-0 text-3 text-dark">
                <b>{30}</b> วัน
              </p>
              <div className="from-group">
                <input
                  type="number"
                  className="form-control text-center mb-1"
                  placeholder="จำนวนผู้ใช้งาน ขั้นต่ำ 30"
                  value={users_count}
                  min={30}
                  onChange={({ target: { value } }) => {
                    store.buy_data.users_count = value
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      )}
    </Observer>
  )
}

export default observer(PackageCustom)
