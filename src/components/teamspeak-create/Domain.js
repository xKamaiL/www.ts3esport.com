import React, { useEffect, useState } from 'react'
import store from './store'
import { observer } from 'mobx-react'

const Domain = () => {
  return (
    <div className="row mb-5">
      <div className="col-md-8">
        <div className="input-group d-flex justify-content-between">
          <input
            type="text"
            className="form-control d-flex"
            maxLength={20}
            minLength={3}
            onChange={({ target: { value } }) => {
              store.buy_data.ip_address = value
              store.domain_isvalid = false
            }}
            placeholder="ตั้งชื่อไอพี"
            value={store.buy_data.ip_address}
          />
          <div className="input-group-append" style={{ width: '30%' }}>
            <select
              className="form-control"
              onChange={({ target: { value } }) => {
                store.buy_data.domain = value
                store.domain_isvalid = false
              }}
            >
              {store.domains.map((item, key) => (
                <option value={item} key={key} selected={key === 0}>
                  {item}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
      <div className="col-md-4">
        <button
          onClick={e => {
            e.preventDefault()
            store.checkDomainIsValid()
          }}
          className="btn  btn-rounded btn-tertiary btn-block"
        >
          <i className="fas fa-search" /> ตรวจสอบ
        </button>
      </div>
    </div>
  )
}

export default observer(Domain)
