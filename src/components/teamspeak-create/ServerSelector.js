import React, { useEffect, useState } from 'react'
import store from './store'
import { observer } from 'mobx-react'

const ServerItem = ({ onClick, active, name, slot, max }) => {
  return (
    <div className="col-md-4">
      <div
        class="featured-box featured-box-effect-2 featured-box-full-primary"
        style={{
          cursor: 'pointer',
          opacity: active ? '0.8' : 1
        }}
        onClick={onClick}
      >
        <div class="box-content box-content-border-bottom">
          {active ? (
            <i
              class="icon-featured fas fa-check text-success"
              style={{ marginTop: '-70px' }}
            />
          ) : (
            <i
              class="icon-featured fas fa-server"
              style={{ marginTop: '-70px' }}
            />
          )}

          <h4 class="font-weight-normal text-5">
            <strong class="font-weight-extra-bold">{name}</strong>
          </h4>

          <p class="mb-2 mt-2 text-2">
            Slot: {slot} / {max}
          </p>
        </div>
      </div>
    </div>
  )
}

const ServerSelector = () => {
  return (
    <div className="row d-flex justify-content-center">
      {store.available_server.map(item => (
        <ServerItem
          key={item.id}
          max={item.max}
          name={item.name}
          slot={item.slot}
          onClick={e => {
            e.preventDefault()
            store.buy_data.server_id = item.id
          }}
          active={item.id === store.buy_data.server_id}
        />
      ))}
    </div>
  )
}

export default observer(ServerSelector)
