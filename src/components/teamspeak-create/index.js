import React, { useEffect, useState } from 'react'
import PackageService from '../../services/PackageService'
import PackageItem from './PackageItem'
import PackageCustom from './PackageCustom'
import PackageSelector from './PackageSelector'
import store from './store'
import { Observer } from 'mobx-react'
import ServerSelector from './ServerSelector'
import Domain from './Domain'
import Table from './Table'
import Preloading from './Preloading'
import userStore from '../userStore'

export const PageHeader = ({ children }) => (
  <section class='page-header page-header-classic'>
    <div class='container'>
      <div class='row'>
        <div class='col p-static'>
          <span class='page-header-title-border visible' />
          <h1>{children}</h1>
        </div>
      </div>
    </div>
  </section>
)

const TeamspeakCreate = () => {
  const [loading, setloading] = useState(true)
  useEffect(() => {
    const fetchData = async () => {
      await store.fetchPackages()
      await store.fetchServers()
      await store.fetchDomains()
      setloading(false)
    }
    document.title = 'เช่าเซิฟเวอร์ทีเอสสาม'
    fetchData()
    return () => {
      store.buy_data = {
        users_count: null,
        domain: '',
        ip_address: '',
        server_id: '',
        is_selected: true,
        plan_id: null
      }
      store.domain_isvalid = false
    }
  }, [])
  if (loading) return <Preloading />
  const isValid = () => {
    /*
     if (store.buy_data.is_selected === null) {
      return false
    }
    if (
      store.buy_data.is_selected === true &&
      store.buy_data.plan_id === null
    ) {
      return false
    }
    */
    if (store.buy_data.plan_id === null) {
      return false
    }
    /*
    if (
      store.buy_data.is_selected === false &&
      store.buy_data.users_count < '30'
    ) {
      return false
    }
    */
    if (store.buy_data.ip_address.length < 3) {
      return false
    }
    return true
  }
  return (
    <Observer>
      {() => (
        <React.Fragment>
          <PageHeader>New Teamspeak 3 Server</PageHeader>
          <div className='container'>
            {userStore.userData.has_free == '1' && (
              <div class='alert alert-success'>
                <strong>ยินดีด้วย!</strong> คุณมีสิทธ์ในการเปิดใช้งานฟรี 1 ครั้ง
                แต่จะไม่สามารถใช้สิทธ์ดังกล่าวกับเซิฟเวอร์ เวอร์ชั่นใหม่ได้
              </div>
            )}
            <div class='heading heading-border heading-middle-border'>
              <h2 class='font-weight-normal'>
                1.) เลือก<strong class='font-weight-extra-bold'>แพคเกจ</strong>
              </h2>
            </div>
            <PackageSelector />
            <div class='heading heading-border heading-middle-border'>
              <h2 class='font-weight-normal'>
                2.) เลือก
                <strong class='font-weight-extra-bold'>เครื่องเซิฟเวอร์</strong>
              </h2>
            </div>
            <ServerSelector />
            <div class='heading heading-border heading-middle-border'>
              <h2 class='font-weight-normal'>
                3.) ตั้งชื่อ<strong class='font-weight-extra-bold'>ไอพี</strong>{' '}
                {store.domain_isvalid && (
                  <i className='fas fa-check text-success' />
                )}
              </h2>
            </div>
            <Domain />
            <div class='heading heading-border heading-middle-border'>
              <h2 class='font-weight-normal'>
                4.) สรุป<strong class='font-weight-extra-bold'>รายการ</strong>
              </h2>
            </div>
            <Table />
            <div className='d-flex justify-content-between mb-2'>
              <button
                disabled={!isValid()}
                onClick={store.submitBuyTeamspeak}
                className='btn btn-lg btn-square btn-success mb-2 btn-block'
              >
                <i className='fas fa-shopping-cart' /> ยืนยันสั่งซื้อ{' '}
                {userStore.userData.has_free == '1' && <>ฟรีครั้งแรก</>}
              </button>
            </div>
          </div>
        </React.Fragment>
      )}
    </Observer>
  )
}

export default TeamspeakCreate
