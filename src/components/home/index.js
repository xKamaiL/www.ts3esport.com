import React, { Component } from 'react'
import SliderImages from './SliderImage'
import ImageService from '../../services/ImageService'
import { FacebookProvider, Page } from 'react-facebook'

class Home extends Component {
    state = {
        items: []
    }

    componentDidMount() {
        document.title = 'หน้าแรก'
        ImageService.getSlide()
            .then(({ data }) => {
                this.setState({
                    items: data
                })
            })
            .catch((_) => {
                this.setState({
                    items: []
                })
            })
    }

    render() {
        return (
            <div className="container">
                <SliderImages items={this.state.items}/>
                <div className="row">
                    <div className="col-6 mt-3 d-flex justify-content-center">
                        <FacebookProvider appId="asd">
                            <Page
                                href="https://www.facebook.com/ts3esports/"
                                tabs="timeline"
                                width={600}
                                height={500}
                            />
                        </FacebookProvider>
                    </div>
                    <div className="col-6 mt-3 d-flex justify-content-center">
                        <iframe
                            src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fts3esports%2Fvideos%2F2457899817870205%2F&show_text=1&width=476"
                            width="600"
                            height="500"
                            style={{
                                border: 'none',
                                overflow: 'hidden'
                            }}
                            scrolling="no"
                            frameBorder={0}
                            allowFullScreen
                        />
                    </div>
                </div>
                {/*
                 <div class="home-intro home-intro-compact bg-primary" id="home-intro">
                 <div class="container">
                 <div class="row align-items-center">
                 <div class="col-lg-8">
                 <p>
                 The fastest way to grow your business with the leader in{' '}
                 <span class="highlighted-word">Technology</span>
                 <span>Check out our options and features included.</span>
                 </p>
                 </div>
                 <div class="col-lg-4">
                 <div class="get-started text-left text-lg-right">
                 <a
                 href="#"
                 class="btn btn-dark btn-lg text-3 font-weight-semibold px-4 py-3"
                 >
                 Get Started Now
                 </a>
                 <div class="learn-more">
                 or <a href="index.html">learn more.</a>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>
                 */}
            </div>
        )
    }
}

export default Home
