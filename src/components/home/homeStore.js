import { action } from 'mobx';

class Store {
  @action
  getPackage = () => {};
}

const homeStore = new Store();

export default homeStore;
