import React, { useEffect, useState, useContext } from 'react'
import { Link, withRouter } from 'react-router-dom'
import UserService from '../../services/UserService'
import { NotificationManager } from 'react-notifications'

const ResetPassword = ({ match, history }) => {
  const code = history.location.search.replace('?code=', '')
  const [loading, setLoading] = useState(false)
  const [newPassword, setNewPassword] = useState('')
  const [newConfirmationPassword, setNewConfirmationPassword] = useState('')
  const submit = async e => {
    e.preventDefault()
    setLoading(true)
    await UserService.reset({
      password: newPassword,
      password_confirmation: newConfirmationPassword,
      code
    })
      .then(res => res.data)
      .then(data => {
        setNewConfirmationPassword('')
        setNewPassword('')
        NotificationManager.success(data.message, 'เปลียนรหัสผ่านสำเร็จ')
        history.push('/')
      })
      .catch(error => {})
    setLoading(false)
  }
  return (
    <div>
      <div class='row justify-content-md-center'>
        <div class='col-md-6 mt-5'>
          <div class='featured-box featured-box-primary text-left mt-0'>
            <div class='box-content'>
              <h4 class='mb-4 color-primary font-weight-semibold text-4 text-uppercase mb-0'>
                รีเซ็ตรหัสผ่าน ?
              </h4>
              <form onSubmit={submit}>
                <div class='form-group '>
                  <label class='font-weight-bold text-dark text-2'>
                    รหัสผ่านใหม่
                  </label>
                  <input
                    type='password'
                    value={newPassword}
                    class='form-control'
                    required
                    onChange={({ target: { value } }) => {
                      setNewPassword(value)
                    }}
                    disabled={loading}
                  />
                </div>
                <div class='form-group '>
                  <label class='font-weight-bold text-dark text-2'>
                    ยืนยันรหัสผ่านใหม่
                  </label>
                  <input
                    type='password'
                    value={newConfirmationPassword}
                    class='form-control'
                    required
                    onChange={({ target: { value } }) => {
                      setNewConfirmationPassword(value)
                    }}
                    disabled={loading}
                  />
                </div>
                <button type='submit' className='btn btn-primary'>
                  <i className='fas fa-check'></i> เปลียนรหัสผ่าน
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default withRouter(ResetPassword)
