import React, { useEffect, useState } from 'react'
import { Observer } from 'mobx-react'
import { withRouter, Link } from 'react-router-dom'
import userStore from '../userStore'
import Recaptcha from 'reaptcha'
import { NotificationManager } from 'react-notifications'

const RegisterPage = () => {
  const [recaptchaInstance, setrecaptchaInstance] = useState(null)
  const [loading, setLoading] = useState(false)
  const [captcha, setcaptcha] = useState(false)
  const [verify, setverify] = useState(false)
  const onloadCallback = function() {
    setcaptcha(true)
    recaptchaInstance.renderExplicitly()
  }
  const submit = async event => {
    event.preventDefault()
    setLoading(true)
    if ((userStore.register_data.token = '')) {
      NotificationManager.error('', 'กรุณายืนยันตัวตน')
    } else {
      userStore.register_data.token = verify
      await userStore.registerUser()
    }
    recaptchaInstance.reset()
    setLoading(false)
  }
  const handleInput = ({ target: { name, value } }) => {
    userStore.register_data[name] = value
  }
  useEffect(() => {
    document.title = 'สมัครสมาชิก'
  }, [])
  const verifyCallback = e => {
    if (loading === false) {
      setverify(e)
    }
  }
  return (
    <Observer>
      {() => (
        <div className='container'>
          <div className='col'>
            <div className='featured-box featured-box-warning text-left mt-5'>
              <div className='box-content p-3'>
                <h4 class='color-danger font-weight-semibold mb-4 text-5 text-uppercase'>
                  Create new account.
                </h4>
                <form onSubmit={submit}>
                  <div className='row'>
                    <div class='form-group col'>
                      <label class='font-weight-bold text-dark text-2'>
                        Username
                      </label>
                      <input
                        type='text'
                        value={userStore.register_data.username}
                        name='username'
                        placeholder='ชื่อผู้ใช้'
                        class='form-control '
                        required
                        onChange={handleInput}
                        disabled={loading}
                      />
                    </div>
                    <div class='form-group col'>
                      <label class='font-weight-bold text-dark text-2'>
                        E-mail Address
                      </label>
                      <input
                        type='email'
                        value={userStore.register_data.email}
                        name='email'
                        placeholder='ที่อยู่อีเมล์'
                        class='form-control'
                        required
                        onChange={handleInput}
                        disabled={loading}
                      />
                    </div>
                  </div>
                  <div className='row'>
                    <div className='form-group col'>
                      <label class='font-weight-bold text-dark text-2'>
                        Password
                      </label>
                      <input
                        type='password'
                        value={userStore.register_data.password}
                        name='password'
                        placeholder='ตั้งรหัสผ่าน อย่างน้อย 6 ตัวอักษร'
                        class='form-control '
                        required
                        onChange={handleInput}
                        disabled={loading}
                      />
                    </div>
                    <div className='form-group col'>
                      <label class='font-weight-bold text-dark text-2'>
                        Confirm Password
                      </label>
                      <input
                        type='password'
                        value={userStore.register_data.password_confirmation}
                        name='password_confirmation'
                        placeholder='ยืนยันรหัสผ่าน'
                        class='form-control '
                        required
                        onChange={handleInput}
                        disabled={loading}
                      />
                    </div>
                  </div>
                  <div className='row'>
                    <div className='form-group col'>
                      <label class='font-weight-bold text-dark text-2'>
                        Telephone
                      </label>
                      <input
                        type='text'
                        value={userStore.register_data.tel}
                        name='tel'
                        placeholder='เบอร์โทรศัพท์'
                        class='form-control '
                        required
                        maxLength={10}
                        onChange={handleInput}
                        disabled={loading}
                      />
                    </div>
                    <div className='form-group col'>
                      <label class='font-weight-bold text-dark text-2'>
                        รหัสยืนยันตัวตน 4 ตัว
                      </label>
                      <input
                        type='password'
                        value={userStore.register_data.pincode}
                        name='pincode'
                        placeholder='รหัสยืนยันตัวตน 4 ตัว กรณีต้องการ ลบ หรือ ปิดเซิฟเวอร์'
                        class='form-control '
                        maxLength={4}
                        required
                        onChange={handleInput}
                      />
                    </div>
                  </div>
                  <div className='d-flex justify-content-center'>
                    <Recaptcha
                      sitekey='6Lei5J4UAAAAANPbB-wt76CXNghtbOfeGOi0wESO'
                      onLoad={onloadCallback}
                      onVerify={verifyCallback}
                      explicit
                      ref={e => {
                        setrecaptchaInstance(e)
                      }}
                    />
                  </div>
                  <div className='d-flex justify-content-center mt-4'>
                    <button
                      className='btn btn-secondary rounded-0 mb-2 btn-lg '
                      type='submit'
                      disabled={!verify || loading}
                    >
                      สมัครสมาชิก
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      )}
    </Observer>
  )
}

export default RegisterPage
