import React, { useEffect, useState } from 'react'
import PaymentService from '../../services/PaymentService'
import userStore from '../userStore'

const TopupHistory = () => {
  const [loading, setLoading] = useState(true)
  const [history, setHistory] = useState([])
  useEffect(() => {
    PaymentService.history()
      .then(({ data }) => {
        setHistory(data)
      })
      .catch(_ => {
        setHistory([])
      })
    return () => {}
  }, [])

  useEffect(() => {
    let a = setInterval(() => {
      PaymentService.history()
        .then(({ data }) => {
          setHistory(data)
          userStore.getProfile()
        })
        .catch(_ => {
          setHistory([])
        })
    }, 3000)
    return () => {
      clearInterval(a)
    }
  }, [])
  return (
    <React.Fragment>
      <h4>ประวัติการเติมเงิน</h4>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>รหัสบัตร / เลขที่อ้างอิง</th>
            <th>จำนวนที่เติม</th>
            <th>สถานะ</th>
            <th className="text-right">เมื่อ</th>
          </tr>
        </thead>
        <tbody>
          {history.map((item, key) => (
            <tr key={key}>
              <td>{key + 1}</td>
              <td>{item.serial}</td>
              <td>{item.amount} ฿</td>
              <td>
                {item.status === 0 ? (
                  <p className="text-muted">รอการตรวจสอบ...</p>
                ) : item.status === 1 ? (
                  <p className="text-success">เติมเงินสำเร็จ</p>
                ) : (
                  <p className="text-danger">ไม่สำเร็จ</p>
                )}
              </td>
              <td className="text-right">{item.created_at}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </React.Fragment>
  )
}

export default TopupHistory
