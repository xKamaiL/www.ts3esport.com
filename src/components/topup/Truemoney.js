import React, { useEffect, useState } from 'react'
import { NotificationManager } from 'react-notifications'
import PaymentService from '../../services/PaymentService'

const TruemoneyFormTopup = () => {
  const [truemoney, setTruemoney] = useState('')
  const submit = e => {
    e.preventDefault()
    if (truemoney.length !== 14) {
      NotificationManager.info('กรุณากรอกให้ครบ 14 หลัก', 'คำเตือน')
    } else {
      PaymentService.truemoney(truemoney)
        .then(({ data }) => {
          NotificationManager.info('รอการตรวจสอบ...', 'เติมเงินสำเร็จ')
          setTruemoney('')
        })
        .catch(_ => {})
    }
  }
  useEffect(() => {
    return () => {}
  }, [])
  return (
    <div class="featured-box featured-box-primary text-left mt-0">
      <div class="box-content">
        <h4 class="color-primary font-weight-semibold  text-uppercase mb-0 text-8 mb-2">
          Truemoney
        </h4>
        <form id="frmLostPassword" class="needs-validation" onSubmit={submit}>
          <div class="form-row">
            <div class="form-group col">
              <label class="font-weight-bold text-dark text-2">
                Truemoney 14 หลัก
              </label>
              <input
                type="text"
                class="form-control form-control-lg"
                placeholder="TRUEMONEY"
                maxLength={14}
                required
                value={truemoney}
                onChange={({ target: { value } }) => {
                  setTruemoney(value)
                }}
              />
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col">
              <button
                type="submit"
                className="btn btn-primary btn-modern float-right"
              >
                เติมเงิน
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  )
}

export default TruemoneyFormTopup
