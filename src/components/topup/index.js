import React, { useEffect, useState, Fragment } from 'react'
import userStore from '../userStore'
import { Observer } from 'mobx-react'
import TopupHistory from './History'
import TruemoneyFormTopup from './Truemoney'
const TopupPage = () => {
  useEffect(() => {
    document.title = 'เติมเงิน'
    return () => {}
  }, [])
  return (
    <Observer>
      {_ => (
        <Fragment>
          <section class="page-header page-header-classic">
            <div class="container">
              <div class="row">
                <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                  <h1>Topup</h1>
                </div>
              </div>
            </div>
          </section>
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <TruemoneyFormTopup />
              </div>
              <div className="col-md-6">
                <TopupHistory />
              </div>
            </div>
          </div>
        </Fragment>
      )}
    </Observer>
  )
}

export default TopupPage
