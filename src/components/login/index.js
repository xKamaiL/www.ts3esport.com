import React, { useState, useEffect } from 'react'
import { withRouter, Link } from 'react-router-dom'
import { observer, Observer } from 'mobx-react'
import userStore from '../userStore'
import styled from 'styled-components'
import { routingStore } from '../common/App'

const LoginWrapper = styled.div`
  height: 100vh;
  .login-form-forgot {
    float: right;
  }
  .login-form-button {
    width: 100%;
  }
  form {
    width: 400px;
  }
`

const LoginPage = ({ history }) => {
  const [loading, setLoading] = useState(false)
  const submit = async event => {
    event.preventDefault()
    setLoading(true)
    await userStore.submitLogin()
    setLoading(false)
  }
  const handleInput = ({ target: { name, value } }) => {
    userStore.login_data[name] = value
  }
  useEffect(() => {
    document.title = 'เข้าสู่ระบบ'
    userStore.getProfile().then(() => {
      if (userStore.isLogin) {
        history.replace('/')
      }
    })
  }, [history])

  return (
    <Observer>
      {props => (
        <LoginWrapper className="d-flex justify-content-center align-items-center" />
      )}
    </Observer>
  )
}

export default withRouter(LoginPage)
