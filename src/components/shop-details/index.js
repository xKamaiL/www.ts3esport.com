import React, { useEffect, useState, useContext } from 'react'
import ShopService from '../../services/ShopService'
import { Link, withRouter } from 'react-router-dom'
import Skeleton from 'react-skeleton-loader'
import ShopItem from '../shop-all/ShopItem'
const ShopDetails = ({ match: { params } }) => {
  const [loading, setLoading] = useState(true)
  const [shopData, setShopData] = useState(null)
  const [isError, setIsError] = useState(false)
  useEffect(() => {
    async function fetch() {
      await ShopService.fetchDetails(params.id)
        .then(({ data }) => {
          setShopData(data)
          document.title = data.name
          setIsError(false)
        })
        .catch(error => {
          setIsError(true)
          if (error.response.status == 404) {
            history.push('/not-found')
          }
        })
      setLoading(false)
    }
    fetch()
    return () => {}
  }, [])

  if (loading)
    return (
      <div className='main shop py-4'>
        <div className='container'>
          <div className='row'>
            <div className='col-6'>
              <Skeleton height='555px' width='100%' widthRandomness='0' />
            </div>
          </div>
        </div>
      </div>
    )
  if (isError) return <p> เกิดข้อผิดพลาด</p>
  return (
    <div className='main shop py-4'>
      <div className='container'>
        <div className='row'>
          <div className='col-lg-6'>
            <img
              alt=''
              width='100%'
              src={
                shopData.logo_image_url ||
                '/assets/img/products/product-grey-7.jpg'
              }
              height='555'
            />
          </div>
          <div className='col-lg-6'>
            <div class='summary entry-summary'>
              <h1 class='mb-0 font-weight-bold text-7'>{shopData.name}</h1>
              <p class='price'>
                <span class='amount'>{shopData.price} ฿</span>
              </p>
              <div
                class='mb-4'
                dangerouslySetInnerHTML={{
                  __html: shopData.description
                }}
              ></div>
              <div class='product-meta'>
                <span class='posted-in'>
                  ติดต่อซื้อสินค้าได้ที่:{' '}
                  <b className='text-primary'>{shopData.user.contact}</b>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className='row products product-thumb-info-list mt-3'>
          <div className='col-12'>
            <hr class='solid my-5' />
            <h4 class='mb-3'>
              สินค้า <strong>ที่เกี่ยวข้อง</strong>
            </h4>
          </div>
          {shopData.related.map(shop => (
            <ShopItem {...shop} key={shop.id} />
          ))}
        </div>
      </div>
    </div>
  )
}

export default withRouter(ShopDetails)
