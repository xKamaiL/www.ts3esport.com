import React, { useEffect, useState } from 'react'
import ServerService from '../../services/ServerService'
import ServerItem from './ServerItem'
import LoadingServerList from './Loading'
const Header = () => (
  <section class="page-header page-header-classic">
    <div class="container">
      <div class="row">
        <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
          <h1 data-title-border>My Server</h1>
        </div>
      </div>
    </div>
  </section>
)

const TeamspeakServerList = () => {
  const [loading, setLoading] = useState(true)
  const [server, setServer] = useState({
    items: []
  })
  useEffect(() => {
    document.title = 'เซิฟเวอร์ของฉัน'
    ServerService.getMyServers()
      .then(({ data }) => {
        setServer(data)
        setLoading(false)
      })
      .catch(_ => {
        setServer({ items: [] })
        setLoading(false)
      })
    return () => {}
  }, [])
  if (loading) return <LoadingServerList />
  return (
    <>
      <Header />
      <div className="container">
        <div className="pricing-table mb-4">
          {server.items.map(item => (
            <ServerItem {...item} key={item.id} />
          ))}
          {server.items.length === 0 && (
            <p className="text-center py-5 mx-auto">
              {' '}
              <i className="fas fa-times" /> ยังไม่มีเซิฟเวอร์
            </p>
          )}
        </div>
      </div>
    </>
  )
}

export default TeamspeakServerList
