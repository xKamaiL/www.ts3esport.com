import React, { useEffect, useState } from 'react'
import Skeleton from 'react-skeleton-loader'

const LoadingServerList = () => {
  return (
    <>
      <section class="page-header page-header-classic">
        <div class="container">
          <div class="row">
            <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
              <h1 data-title-border>My Server</h1>
            </div>
          </div>
        </div>
      </section>
      <div className="container">
        <div className="pricing-table mb-4">
          <div className="col-md-3">
            <Skeleton width="100%" widthRandomness="0" height="370px" />
          </div>
          <div className="col-md-3">
            <Skeleton width="100%" widthRandomness="0" height="370px" />
          </div>
          <div className="col-md-3">
            <Skeleton width="100%" widthRandomness="0" height="370px" />
          </div>
          <div className="col-md-3">
            <Skeleton width="100%" widthRandomness="0" height="370px" />
          </div>
        </div>
      </div>
    </>
  )
}

export default LoadingServerList
