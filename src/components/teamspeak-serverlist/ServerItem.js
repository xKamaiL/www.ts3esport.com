import React, { useEffect, useState } from 'react'
import { withRouter, Link } from 'react-router-dom'
const ServerItem = ({
  ip_address,
  maxclients,
  id,
  expireMessageTh,
  createMessageTh
}) => {
  return (
    <div class="col-md-6 col-lg-3">
      <div class="plan">
        <div class="plan-header font-weight-light">
          <h3>{ip_address}</h3>
        </div>
        <div class="plan-price">
          <span class="price">{maxclients}</span>
          <label class="price-label">ผู้ใช้งาน</label>
        </div>
        <div class="plan-features">
          <ul>
            <li>
              หมดอายุ <b>{expireMessageTh}</b>
            </li>
            <li>
              เช่าเมื่อ <b>{createMessageTh}</b>
            </li>
            <li>
              <a href={'ts3server://' + ip_address}>เข้าใช้งาน</a>
            </li>
          </ul>
        </div>
        <div class="plan-footer p-3">
          <Link
            to={'/teamspeak/manage/' + id + '/home'}
            class="btn btn-lg btn-square btn-dark btn-outline  btn-block"
          >
            จัดการ
          </Link>
        </div>
      </div>
    </div>
  )
}

export default ServerItem
