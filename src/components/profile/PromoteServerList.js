import React, { useEffect, useState, useContext } from 'react'
import PromoteService from '../../services/PromoteService'
import { Link, withRouter } from 'react-router-dom'
const PromoteServerList = () => {
  const [promoteServer, setPromoteServer] = useState([])
  useEffect(() => {
    async function fetchData() {
      await PromoteService.me().then(({ data }) => {
        setPromoteServer(data)
      })
    }
    fetchData()
    return () => {}
  }, [])
  return (
    <table class='table table-striped'>
      <thead>
        <tr>
          <th>#</th>
          <th>ชื่อ</th>
          <th>อันดับ</th>
          <th>คะแนนโหวต</th>
          <th>กระทำ</th>
        </tr>
      </thead>
      <tbody>
        {promoteServer.map(server => (
          <tr>
            <td>{server.id}</td>
            <td>{server.name}</td>
            <td>
              ทั้งหมด: {server.all_ranking} (เกม: {server.game_ranking})
            </td>
            <td className='text-center'>{server.total_latest_vote}</td>
            <td>
              <Link to={`/profile/promote-server/${server.id}`}>แก้ไข/ลบ</Link>
            </td>
          </tr>
        ))}
        {promoteServer.length === 0 && (
          <tr>
            <td colSpan={5}>
              <p className='text-center'>ไม่มี</p>
            </td>
          </tr>
        )}
      </tbody>
    </table>
  )
}

export default PromoteServerList
