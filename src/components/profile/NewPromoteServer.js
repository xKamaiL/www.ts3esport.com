import React, { useEffect, useState, useContext } from 'react'
import userStore from '../userStore'
import _ from 'lodash'
import GameService from '../../services/GameService'
import 'froala-editor/js/plugins.pkgd.min.js'
import PromoteService from '../../services/PromoteService'
import { NotificationManager } from 'react-notifications'
import { Link, withRouter } from 'react-router-dom'
import { convertFromHTML, ContentState } from 'draft-js'
import { EditorState, convertToRaw } from 'draft-js'
import draftToHtml from 'draftjs-to-html'
import TextEditor from '../ui/TextEditor'

const NewPromoteServer = ({ match: { params }, history }) => {
  const isEditing = !_.isNil(params.id)
  const [games, setGames] = useState([])
  const [currentGames, setCurrentGames] = useState(null)
  const [name, setName] = useState('')
  const [description, setDescription] = useState(EditorState.createEmpty())
  const [websiteUrl, setWebsiteUrl] = useState('')
  const [facebookUrl, setFacebookUrl] = useState('')
  const [fbGroupUrl, setFbGroupUrl] = useState('')
  const [loading, setLoading] = useState(isEditing)
  const [fileImage, setFileImage] = useState(null)
  const [isError, setIsError] = useState(false)
  const setModelEditor = e => {
    setDescription(e)
  }
  useEffect(() => {
    async function fetch() {
      await GameService.fetchGames()
        .then(res => res.data)
        .then(data => {
          setGames(data)
        })
        .catch(error => {
          setGames([])
          alert('เกิดข้อผิดพลาดไม่สามารถเรียกดูเกมได้')
        })
      if (isEditing) {
        await PromoteService.fetchDetails(params.id)
          .then(({ data }) => {
            setName(data.name)
            const convertedState = convertFromHTML(data.description)
            setDescription(
              EditorState.createWithContent(
                ContentState.createFromBlockArray(
                  convertedState.contentBlocks,
                  convertedState.entityMap
                )
              )
            )
            setWebsiteUrl(data.website_url)
            setFacebookUrl(data.fanpage_url)
            setFbGroupUrl(data.fb_group_url)
            setCurrentGames(data.game_id)
            setLoading(false)
          })
          .catch(error => {
            console.log(error)
            if (error.response.status === 404) {
              history.push('/profile')
            } else {
              NotificationManager.danger('', 'เกิดข้อผิดพลาด')
            }
          })
      } else {
        setLoading(false)
      }
    }
    document.title = 'โปรโมทเซิฟเวอร์ของคุณ'
    fetch()
    return () => {}
  }, [])
  const submitDeletePromoteServer = e => {
    e.preventDefault()
    return PromoteService.delete(params.id)
      .then(_ => {
        NotificationManager.success('', 'ลบเซิฟเวอร์ที่โปรโมทเรียบร้อย')
        history.push('/profile')
      })
      .catch(err => {})
  }
  if (isEditing && loading) {
    return null
  }
  const submit = async event => {
    event.preventDefault()
    const payload = new FormData()

    if (currentGames === null) {
      return NotificationManager.warning('', 'กรุณาเลือกเกมส์')
    }
    payload.append(
      'description',
      draftToHtml(convertToRaw(description.getCurrentContent()))
    )
    payload.append('name', name)
    payload.append('logo_image_url', fileImage)
    payload.append('website_url', websiteUrl)
    payload.append('fb_group_url', fbGroupUrl)
    payload.append('fanpage_url', facebookUrl)
    payload.append('game_id', currentGames)
    if (!isEditing) {
      if (_.isNull(fileImage)) {
        return NotificationManager.succes('', 'กรุณาอัพโหลดไฟล์ภาพ')
      }
      await PromoteService.create(payload)
        .then(res => res.data)
        .then(data => {
          NotificationManager.success('', 'โปรโมทเซิฟเวอร์เรียบร้อย')
          history.push('/profile')
        })
        .catch(error => {
          NotificationManager.warning('', 'ไม่สำเร็จ')
        })
    } else {
      //    history.push('/profile')
      if (_.isNull(fileImage)) payload.delete('logo_image_url')
      if (_.isNull(facebookUrl)) payload.delete('fanpage_url')
      if (_.isNull(fbGroupUrl)) payload.delete('fb_group_url')
      if (_.isNull(websiteUrl)) payload.delete('website_url')
      await PromoteService.update(params.id, payload)
        .then(res => res.data)
        .then(data => {
          NotificationManager.success('', 'บันทึกการเปลียนแปลงเรียบร้อย')
          history.push('/profile')
        })
        .catch(error => {
          NotificationManager.warning('', 'ไม่สำเร็จ')
        })
    }
  }
  return (
    <>
      <section class='page-header page-header-classic'>
        <div class='container'>
          <div class='row'>
            <div class='col p-static'>
              <span class='page-header-title-border visible' />
              <h1>
                {isEditing
                  ? 'แก้ไขการโปรโมทเซิฟเวอร์ของคุณ'
                  : 'โปรโมทเซิฟเวอร์ของคุณ'}
              </h1>
            </div>
          </div>
        </div>
      </section>
      <div className='container'>
        <form action='' onSubmit={submit}>
          <div className='form-group'>
            <label>ชื่อเซิฟเวอร์</label>
            <input
              type='text'
              className='form-control'
              value={name}
              onChange={({ target: { value } }) => {
                setName(value)
              }}
            />
          </div>
          <div className='form-group'>
            <label>รูปภาพ Logo</label>
            <input
              type='file'
              className='form-control'
              onChange={({ target: { files } }) => {
                setFileImage(files[files.length - 1])
              }}
            />
          </div>
          <div className='form-group'>
            <label>เลือกเกมส์</label>
            <select
              className='form-control'
              value={currentGames}
              onChange={({ target: { value } }) => {
                setCurrentGames(value)
              }}
            >
              <option readOnly disabled selected>
                กรุณาเลือกเกมส์
              </option>
              {games.map(a => (
                <option value={a.id}>{a.name}</option>
              ))}
            </select>
          </div>
          <div className='form-group'>
            <label>เนื้อหา</label>
            <TextEditor editorState={description} onChange={setModelEditor} />
          </div>
          <div className='form-group'>
            <label>ที่อยู่เว็บไซต์ (Website url)</label>
            <input
              type='text'
              className='form-control'
              value={websiteUrl}
              onChange={({ target: { value } }) => {
                setWebsiteUrl(value)
              }}
            />
          </div>
          <div className='form-group'>
            <label>Facebook Fanpage Link</label>
            <input
              type='text'
              className='form-control'
              value={facebookUrl}
              onChange={({ target: { value } }) => {
                setFacebookUrl(value)
              }}
            />
          </div>
          <div className='form-group'>
            <label>Facebook Group Link</label>
            <input
              type='text'
              className='form-control'
              value={fbGroupUrl}
              onChange={({ target: { value } }) => {
                setFbGroupUrl(value)
              }}
            />
          </div>
          <button type='submit' className='btn btn-block btn-success'>
            {isEditing ? 'บันทึก' : 'ยืนยัน'}
          </button>
          {isEditing && (
            <button
              className='btn btn-block btn-danger'
              onClick={submitDeletePromoteServer}
            >
              ลบ
            </button>
          )}
        </form>
      </div>
    </>
  )
}

export default withRouter(NewPromoteServer)
