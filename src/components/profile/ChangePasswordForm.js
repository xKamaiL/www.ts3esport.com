import React, { useEffect, useState, useContext } from 'react'
import userStore from '../userStore'
import UserService from '../../services/UserService'
import { NotificationManager } from 'react-notifications'
const ChangePasswordForm = () => {
  const [loading, setLoading] = useState(false)
  const [oldPassword, setOldPassword] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [newConfirmationPassword, setNewConfirmationPassword] = useState('')
  const submit = async e => {
    e.preventDefault()
    setLoading(true)
    await UserService.changePassword({
      password: newPassword,
      password_confirmation: newConfirmationPassword,
      old_password: oldPassword
    })
      .then(res => res.data)
      .then(data => {
        setOldPassword('')
        setNewConfirmationPassword('')
        setNewPassword('')
        NotificationManager.success(data.message, 'สำเร็จ')
      })
      .catch(error => {})
    setLoading(false)
  }
  return (
    <form onSubmit={submit}>
      <div class='form-group '>
        <label class='font-weight-bold text-dark text-2'>
          รหัสผ่านปัจจุบัน
        </label>
        <input
          type='password'
          value={oldPassword}
          class='form-control'
          required
          onChange={({ target: { value } }) => {
            setOldPassword(value)
          }}
          disabled={loading}
        />
      </div>
      <div class='form-group '>
        <label class='font-weight-bold text-dark text-2'>รหัสผ่านใหม่</label>
        <input
          type='password'
          value={newPassword}
          class='form-control'
          required
          onChange={({ target: { value } }) => {
            setNewPassword(value)
          }}
          disabled={loading}
        />
      </div>
      <div class='form-group '>
        <label class='font-weight-bold text-dark text-2'>
          ยืนยันรหัสผ่านใหม่
        </label>
        <input
          type='password'
          value={newConfirmationPassword}
          class='form-control'
          required
          onChange={({ target: { value } }) => {
            setNewConfirmationPassword(value)
          }}
          disabled={loading}
        />
      </div>
      <button type='submit' className='btn btn-primary'>
        <i className='fas fa-check'></i> เปลียนรหัสผ่าน
      </button>
    </form>
  )
}

export default ChangePasswordForm
