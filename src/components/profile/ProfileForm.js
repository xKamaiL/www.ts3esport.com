import React, { useEffect, useState, useContext } from 'react'
import userStore from '../userStore'
import UserService from '../../services/UserService'
import { NotificationManager } from 'react-notifications'
import { observer } from 'mobx-react-lite'
const ProfileForm = () => {
  const [loading, setLoading] = useState(false)
  const [imageFile, setImageFile] = useState(null)
  const [contact, setContact] = useState(userStore.userData.contact)
  useEffect(() => {
    return () => {}
  }, [])
  const submit = async e => {
    e.preventDefault()
    setLoading(true)
    const a = new FormData()
    if (imageFile !== null) {
      a.append('profile_image', imageFile)
    }
    a.append('contact', contact)
    await UserService.profile(a)
      .then(res => res.data)
      .then(data => {
        NotificationManager.success('', data.message)
        userStore.getProfile()
      })
      .catch(error => {})
    setLoading(false)
  }
  return (
    <form onSubmit={submit}>
      <div class='form-group '>
        <label class='font-weight-bold text-dark text-2'>รูปประจำตัว</label>
        <input
          type='file'
          class='form-control'
          onChange={({ target: { files } }) => {
            setImageFile(files[files.length - 1])
          }}
          disabled={loading}
        />
      </div>
      <div class='form-group '>
        <label class='font-weight-bold text-dark text-2'>
          ช่องทางการติดต่อ (สำหรับขายของ)
        </label>
        <input
          type='text'
          value={contact}
          class='form-control'
          required
          onChange={({ target: { value } }) => {
            setContact(value)
          }}
          disabled={loading}
        />
      </div>
      <button type='submit' className='btn btn-primary'>
        <i className='fas fa-check'></i> บันทึก
      </button>
    </form>
  )
}

export default observer(ProfileForm)
