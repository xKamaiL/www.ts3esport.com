import React, { useEffect, useState, useContext } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import userStore from '../userStore'
import ChangePasswordForm from './ChangePasswordForm'
import ProfileForm from './ProfileForm'
import PromoteServerList from './PromoteServerList'
import ShopItemLists from './ShopItemList'
const ProfilePage = () => {
  useEffect(() => {
    document.title = 'โปรไฟล์'
    return () => {}
  }, [])
  return (
    <React.Fragment>
      <section class='page-header page-header-classic'>
        <div class='container'>
          <div class='row'>
            <div class='col p-static'>
              <span class='page-header-title-border visible' />
              <h1>โปรไฟล์ , {userStore.userData.username} </h1>
            </div>
          </div>
        </div>
      </section>
      <div className='container'>
        <div className='row'>
          <div className='col-md-6'>
            <div className='featured-box featured-box-warning text-left mt-5'>
              <div className='box-content p-3'>
                <h4 class='color-danger font-weight-semibold mb-4 text-5 text-uppercase'>
                  ข้อมูลของคุณ
                </h4>
                <div className='mx-auto text-center'>
                  <img
                    src={
                      _.isNil(userStore.userData.profile_image)
                        ? 'https://via.placeholder.com/180'
                        : userStore.userData.profile_image
                    }
                    width={180}
                    height={180}
                    className='img-fiuld'
                  />
                </div>
                <ProfileForm />
              </div>
            </div>
          </div>
          <div className='col-md-6'>
            <div className='featured-box featured-box-warning text-left mt-5'>
              <div className='box-content p-3'>
                <h4 class='color-danger font-weight-semibold mb-4 text-5 text-uppercase'>
                  เปลียนรหัสผ่าน
                </h4>
                <ChangePasswordForm />
              </div>
            </div>
          </div>
        </div>
        <div className='row'>
          <div className='col-md-6'>
            <div className='featured-box featured-box-warning text-left mt-5'>
              <div className='box-content p-3'>
                <h4 class='color-danger font-weight-semibold mb-4 text-5 text-uppercase d-flex align-items-center justify-content-between'>
                  รายการเซิฟเวอร์ที่โปรโมท
                  <Link
                    to='/profile/new-promote-server'
                    className='btn btn-success'
                  >
                    <i className='fas fa-plus'></i> เพิ่มรายการ
                  </Link>
                </h4>

                {/* */}
                <PromoteServerList />
              </div>
            </div>
          </div>
          <div className='col-md-6'>
            <div className='featured-box featured-box-warning text-left mt-5'>
              <div className='box-content p-3'>
                <h4 class='color-danger font-weight-semibold mb-4 text-5 text-uppercase d-flex align-items-center justify-content-between'>
                  รายการไอดีเกมส์ที่ลงขาย
                  <Link to='/profile/new-shop-item' className='btn btn-success'>
                    <i className='fas fa-plus'></i> เพิ่มรายการ
                  </Link>
                </h4>
                <ShopItemLists />
                {/* */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default withRouter(observer(ProfilePage))
