import React, { useEffect, useState, useContext } from 'react'
import ShopService from '../../services/ShopService'
import { Link, withRouter } from 'react-router-dom'
const ShopItemLists = () => {
  const [promoteServer, setPromoteServer] = useState([])
  useEffect(() => {
    async function fetchData() {
      await ShopService.me().then(({ data }) => {
        setPromoteServer(data)
      })
    }
    fetchData()
    return () => {}
  }, [])
  return (
    <table class='table table-striped'>
      <thead>
        <tr>
          <th>#</th>
          <th>ชื่อสินค้า</th>
          <th>ราคา</th>
          <th>กระทำ</th>
        </tr>
      </thead>
      <tbody>
        {promoteServer.map(s => (
          <tr>
            <td>{s.id}</td>
            <td>{s.name}</td>
            <td>{s.price} บาท</td>
            <td>
              <Link to={`/profile/shop-item/${s.id}`}>แก้ไข/ลบ</Link>
            </td>
          </tr>
        ))}
        {promoteServer.length === 0 && (
          <tr>
            <td colSpan={4}>
              <p className='text-center'>ไม่มี</p>
            </td>
          </tr>
        )}
      </tbody>
    </table>
  )
}

export default ShopItemLists
