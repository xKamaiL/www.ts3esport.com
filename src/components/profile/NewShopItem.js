import React, { useEffect, useState, useContext } from 'react'
import _ from 'lodash'
import GameService from '../../services/GameService'
import 'froala-editor/js/plugins.pkgd.min.js'
import PromoteService from '../../services/PromoteService'
import { NotificationManager } from 'react-notifications'
import { Link, withRouter } from 'react-router-dom'
import { convertFromHTML, ContentState } from 'draft-js'
import { EditorState, convertToRaw } from 'draft-js'
import draftToHtml from 'draftjs-to-html'
import TextEditor from '../ui/TextEditor'
import ShopService from '../../services/ShopService'

const NewShopItem = ({ match: { params }, history }) => {
  const isEditing = !_.isNil(params.id)
  const [games, setGames] = useState([])
  const [currentGames, setCurrentGames] = useState(null)
  const [name, setName] = useState('')
  const [description, setDescription] = useState(EditorState.createEmpty())
  const [loading, setLoading] = useState(isEditing)
  const [fileImage, setFileImage] = useState(null)
  const [price, setPrice] = useState(0)
  const setModelEditor = e => {
    setDescription(e)
  }
  useEffect(() => {
    async function fetch() {
      await GameService.fetchGames()
        .then(res => res.data)
        .then(data => {
          setGames(data)
        })
        .catch(error => {
          setGames([])
          alert('เกิดข้อผิดพลาดไม่สามารถเรียกดูเกมได้')
        })
      if (isEditing) {
        await ShopService.fetchDetails(params.id)
          .then(({ data }) => {
            setName(data.name)
            const convertedState = convertFromHTML(data.description)
            setDescription(
              EditorState.createWithContent(
                ContentState.createFromBlockArray(
                  convertedState.contentBlocks,
                  convertedState.entityMap
                )
              )
            )
            setCurrentGames(data.game_id)
            setPrice(data.price)
            setLoading(false)
          })
          .catch(error => {
            if (error.response.status === 404) {
              history.push('/profile')
            } else {
              NotificationManager.danger('', 'เกิดข้อผิดพลาด')
            }
          })
      } else {
        setLoading(false)
      }
    }
    document.title = 'ลงขายไอดีเกมส์'
    fetch()
    return () => {}
  }, [])
  const submitDeletePromoteServer = e => {
    e.preventDefault()
    return ShopService.delete(params.id)
      .then(_ => {
        NotificationManager.success('', 'ลบสินค้าดังกล่าวเรียบร้อย')
        history.push('/profile')
      })
      .catch(err => {})
  }
  if (isEditing && loading) {
    return null
  }
  const submit = async event => {
    event.preventDefault()
    const payload = new FormData()
    if (currentGames === null) {
      return NotificationManager.warning('', 'กรุณาเลือกเกมส์')
    }
    payload.append(
      'description',
      draftToHtml(convertToRaw(description.getCurrentContent()))
    )
    payload.append('name', name)
    payload.append('logo_image_url', fileImage)
    payload.append('game_id', currentGames)
    payload.append('albums', JSON.stringify([]))
    payload.append('price', price)
    if (!isEditing) {
      if (_.isNull(fileImage)) {
        return NotificationManager.succes('', 'กรุณาอัพโหลดไฟล์ภาพ')
      }
      await ShopService.create(payload)
        .then(res => res.data)
        .then(data => {
          NotificationManager.success('', 'ลงขายสินค้าดังกล่าวเรียบร้อย')
          history.push('/profile')
        })
        .catch(error => {
          NotificationManager.warning('', 'ไม่สำเร็จ')
        })
    } else {
      //    history.push('/profile')
      if (_.isNull(fileImage)) payload.delete('logo_image_url')
      await ShopService.update(params.id, payload)
        .then(res => res.data)
        .then(_ => {
          NotificationManager.success('', 'บันทึกการเปลียนแปลงเรียบร้อย')
          history.push('/profile')
        })
        .catch(_ => {
          NotificationManager.warning('', 'ไม่สำเร็จ')
        })
    }
  }
  return (
    <>
      <section class='page-header page-header-classic'>
        <div class='container'>
          <div class='row'>
            <div class='col p-static'>
              <span class='page-header-title-border visible' />
              <h1>{isEditing ? 'แก้ไขสินค้าของคุณ' : 'ลงขายไอดีเกมส์'}</h1>
            </div>
          </div>
        </div>
      </section>
      <div className='container'>
        <form action='' onSubmit={submit}>
          <div className='row'>
            <div className='col'>
              <div className='form-group'>
                <label>ชื่อสินค้า</label>
                <input
                  type='text'
                  className='form-control'
                  value={name}
                  onChange={({ target: { value } }) => {
                    setName(value)
                  }}
                />
              </div>
            </div>
            <div className='col'>
              <div className='form-group'>
                <label>ราคาสินค้า</label>
                <input
                  type='number'
                  className='form-control'
                  value={price}
                  onChange={({ target: { value } }) => {
                    setPrice(value)
                  }}
                />
              </div>
            </div>
          </div>
          <div className='form-group'>
            <label>รูปภาพ แนะนำ </label>
            <input
              type='file'
              className='form-control'
              onChange={({ target: { files } }) => {
                setFileImage(files[files.length - 1])
              }}
            />
          </div>
          <div className='form-group'>
            <label>เลือกเกมส์</label>
            <select
              className='form-control'
              value={currentGames}
              onChange={({ target: { value } }) => {
                setCurrentGames(value)
              }}
            >
              <option readOnly disabled selected>
                กรุณาเลือกเกมส์
              </option>
              {games.map(a => (
                <option value={a.id}>{a.name}</option>
              ))}
            </select>
          </div>
          <div className='form-group'>
            <label>เนื้อหา</label>
            <TextEditor editorState={description} onChange={setModelEditor} />
          </div>
          <button type='submit' className='btn btn-block btn-success'>
            {isEditing ? 'บันทึก' : 'ยืนยัน'}
          </button>
          {isEditing && (
            <button
              className='btn btn-block btn-danger'
              onClick={submitDeletePromoteServer}
            >
              ลบ
            </button>
          )}
        </form>
      </div>
    </>
  )
}

export default withRouter(NewShopItem)
