import React from 'react'
import axios from 'axios'
import { NotificationManager } from 'react-notifications'
import userStore from '../components/userStore'
import store from 'store'
import dotEnv from 'dotenv'

dotEnv.config()
// https://api.ts.in.th
const API_URL = `https://api.ts.in.th/api/` || ``
// http://127.0.0.1:8080/api/
export const API_URL_NEW = API_URL
export const requestWithToken = axios.create({
  baseURL: `${API_URL}`,
  timeout: 6000 * 10,
})
export const request = axios.create({
  baseURL: `${API_URL}`,
  timeout: 6000,
})

function errorResponseHandler(error) {
  if (
    error.config.hasOwnProperty('errorHandle') &&
    error.config.errorHandle === false
  ) {
    return Promise.reject(error)
  }
  if (error.response) {
    const { status, data } = error.response
    switch (status) {
      case 422:
        const errors = data.errors
        NotificationManager.error('กรุณากรอกข้อมูลให้ถูกต้อง', 'Failed!')
        Object.keys(errors).map((key) => {
          errors[key].map((message) => {
            NotificationManager.error(message, 'ข้อมูลไม่ถูกต้อง')
          })
        })
        break
      case 401:
        if (!userStore.firstLoadApp) {
          NotificationManager.warning(data.message, 'คำเตือน')
        }
        break
      default:
        NotificationManager.warning(data.message, 'เกิดข้อผิดพลาด')
        break
    }

    return Promise.reject(error)
  } else {
    NotificationManager.error('เกิดข้อผิดพลาดทางเครือข่าย', 'No Connection')
    return Promise.reject(error)
  }
}

requestWithToken.interceptors.request.use(async (config) => {
  config.headers = {
    Authorization: 'Bearer ' + store.get('accessToken'),
    Accept: 'application/json',
  }
  return config
})
requestWithToken.interceptors.response.use(
  (response) => response,
  errorResponseHandler
)
request.interceptors.response.use((response) => response, errorResponseHandler)
